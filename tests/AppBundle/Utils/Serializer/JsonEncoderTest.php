<?php

namespace Tests\AppBundle\Utils\Serializer;

use AppBundle\Utils\Serializer\JsonEncoder;
use PHPUnit\Framework\TestCase;

class JsonEncoderTest extends TestCase
{
    public function testEncode()
    {
        $encoder = new JsonEncoder();

        $result = $encoder->encode(["Foo" => ["Bar", "Bas"]]);

        $this->assertJsonStringEqualsJsonString($result, json_encode(["Foo" => ["Bar", "Bas"]]));
    }
}