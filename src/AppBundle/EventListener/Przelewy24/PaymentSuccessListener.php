<?php



namespace AppBundle\EventListener\Przelewy24;

use Allset\Przelewy24Bundle\Event\PaymentEventInterfce;
use AppBundle\AppSettings;
use AppBundle\Entity\Indent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Class PaymentSuccessListener
 *
 * @package AppBundle\EventListener\Przelewy24
 */
class PaymentSuccessListener
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * PaymentSuccessListener constructor.
     *
     * @param  EntityManagerInterface  $em
     */
    public function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param  PaymentEventInterfce  $event
     *
     * @throws \Twig\Error\Error
     */
    public function onPrzelewy24EventPaymentSuccess(PaymentEventInterfce $event): void
    {
        $token = $event->getPayment()->getSessionId();

        $indent = $this->em->getRepository(Indent::class)->findOneByToken($token);

        if (!$indent || !($indent instanceof Indent)) {
            throw  new \InvalidArgumentException('Nie znaleziono zamówienia o tokenie: '.$token);
        }

        if ($indent->getDisableShipping() && count($indent->getIndentSubscriptions())) {
            $now = new \DateTime();
            foreach ($indent->getIndentSubscriptions() as $indentSubscription) {
                $device = $indentSubscription->getDevice();

                $date = clone $now;
                if ($device->getEndAt() >= $now) {
                    $date = clone $device->getEndAt();
                }

                $date->modify('+'.$indentSubscription->getMonths().' month');
                $device->setEndAt($date);

                $this->em->persist($device);
            }

            if(!$indent->getInvoice()){
                $indent->setEnded(true);
            }
        }

        $indent->setPayed(true);
        $this->em->persist($indent);
        $this->em->flush();

        //--------------- sending mails
        $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Do admina - Opłacone zamówienie'))
                ->setFrom('noreply@z-track.com', 'z-Track')
                ->setTo(AppSettings::ADMIN_EMAIL)
                ->setReplyTo($indent->getEmail())
                ->setBody(
                        $this->templating->render(
                                'emails/admin/indent.html.twig',
                                ['indent' => $indent]
                        ),
                        'text/html'
                );

        $this->mailer->send($message);
        //--------------- sending mails

        //--------------- sending mails
        $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Dziękujemy za opłacenie zamówienia'))
                ->setFrom('noreply@z-track.com', 'z-Track')
                ->setTo($indent->getEmail())
                ->setReplyTo(AppSettings::ADMIN_EMAIL)
                ->setBody(
                        $this->templating->render(
                                'emails/user/indentPayed.html.twig',
                                ['indent' => $indent]
                        ),
                        'text/html'
                );

        $this->mailer->send($message);
        //--------------- sending mails
    }
}
