<?php


namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use AppBundle\Helper\AppHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Indent
 *
 * @package AppBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="indent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IndentRepository")
 */
class Indent
{
    use Base;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $token;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"phone"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"items"})
     * @Assert\Email(checkMX=true)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"invoice"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nip;

    /**
     * @var string
     *
     * @Assert\Length(max="1020")
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $info;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $adminNotes;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $invoice;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $payed;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ended;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disableShipping;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $tempEnded;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"paczkomat"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paczkomat;

    /**
     * @Assert\Valid(groups={"items"})
     * @Assert\NotBlank(message="Dane wysyłki nie mogą być puste",groups={"items"})
     * @ORM\OneToOne(targetEntity="Address", cascade={"all"})
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id")
     */
    private $shippingAddress;

    /**
     * @Assert\Valid(groups={"invoice"})
     * @Assert\NotBlank(groups={"invoice"}, message="Dane do faktury nie mogą być puste")
     * @ORM\OneToOne(targetEntity="Address", cascade={"all"})
     * @ORM\JoinColumn(name="invoice_address_id", referencedColumnName="id")
     */
    private $invoiceAddress;

    /**
     * @Assert\NotBlank(groups={"items"})
     * @ORM\ManyToOne(targetEntity="Shipping")
     * @ORM\JoinColumn(name="shipping_id", referencedColumnName="id")
     */
    private $shipping;

    /**
     * @Assert\NotBlank(groups={"items"})
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="indents")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="IndentItem", mappedBy="indent", cascade={"all"})
     */
    private $indentItems;

    /**
     * @ORM\OneToMany(targetEntity="IndentSubscription", mappedBy="indent", cascade={"all"})
     */
    private $indentSubscriptions;

    /**
     * Indent constructor.
     */
    public function __construct()
    {
        $this->indentItems = new ArrayCollection();
        $this->indentSubscriptions = new ArrayCollection();
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param  float  $total
     *
     * @return Indent
     */
    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param  string  $token
     *
     * @return Indent
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNip(): ?string
    {
        return $this->nip;
    }

    /**
     * @param  string|null  $nip
     *
     * @return Indent
     */
    public function setNip(?string $nip = null): self
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getShippingAddress(): ?Address
    {
        return $this->shippingAddress;
    }

    /**
     * @param  Address|null  $shippingAddress
     *
     * @return Indent
     */
    public function setShippingAddress(?Address $shippingAddress = null): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getInvoiceAddress(): ?Address
    {
        return $this->invoiceAddress;
    }

    /**
     * @param  Address|null  $invoiceAddress
     *
     * @return Indent
     */
    public function setInvoiceAddress(?Address $invoiceAddress = null): self
    {
        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getInvoice(): ?bool
    {
        return $this->invoice;
    }

    /**
     * @param  bool  $invoice
     *
     * @return Indent
     */
    public function setInvoice(bool $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param  string  $phoneNumber
     *
     * @return Indent
     */
    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return Shipping|null
     */
    public function getShipping(): ?Shipping
    {
        return $this->shipping;
    }

    /**
     * @param  Shipping|null  $shipping
     *
     * @return Indent
     */
    public function setShipping(?Shipping $shipping = null): self
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param  Country|null  $country
     *
     * @return Indent
     */
    public function setCountry(?Country $country = null): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @param  string|null  $info
     *
     * @return Indent
     */
    public function setInfo(?string $info = null): self
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdminNotes(): ?string
    {
        return $this->adminNotes;
    }

    /**
     * @param  string|null  $adminNotes
     *
     * @return Indent
     */
    public function setAdminNotes(?string $adminNotes = null): self
    {
        $this->adminNotes = $adminNotes;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaczkomat(): ?string
    {
        return $this->paczkomat;
    }

    /**
     * @param  string|null  $paczkomat
     *
     * @return Indent
     */
    public function setPaczkomat(?string $paczkomat = null): self
    {
        $this->paczkomat = $paczkomat;

        return $this;
    }

    /**
     * @param  IndentItem  $indentItem
     *
     * @return Indent
     */
    public function addIndentItem(IndentItem $indentItem): self
    {
        $this->indentItems[] = $indentItem;

        return $this;
    }

    /**
     * @param  IndentItem  $indentItem
     *
     * @return bool
     */
    public function removeIndentItem(IndentItem $indentItem): bool
    {
        return $this->indentItems->removeElement($indentItem);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndentItems(): \Doctrine\Common\Collections\Collection
    {
        return $this->indentItems;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param  string  $email
     *
     * @return Indent
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPayed(): ?bool
    {
        return $this->payed;
    }

    /**
     * @param  bool|null  $payed
     *
     * @return Indent
     */
    public function setPayed(?bool $payed = null): self
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getEnded(): ?bool
    {
        return $this->ended;
    }

    /**
     * @param  bool|null  $ended
     *
     * @return Indent
     */
    public function setEnded(?bool $ended = null): self
    {
        $this->ended = $ended;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param  User|null  $user
     *
     * @return Indent
     */
    public function setUser(?User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function generateToken(): void
    {
        $this->token = AppHelper::generateToken();
    }

    public function invoiceDefaultToUser()
    {
        $user = $this->getUser();

        if ($user) {
            $user->setPhoneNumber($this->phoneNumber);
            $user->setInvoice($this->getInvoice());
        }

        if ($user && $this->getInvoice()) {
            $user->setInvoiceAddress($this->getInvoiceAddress());
            $user->setNip($this->getNip());
        }
        if ($user && !$this->getInvoice()) {
            $user->setInvoiceAddress(null);
            $user->setNip(null);
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function calculateTotal()
    {
        $this->total = 0;
        foreach ($this->indentItems as $item) {
            if (!($item instanceof IndentItem)) {
                throw  new \InvalidArgumentException();
            }
            $this->total += $item->getPrice() * $item->getCount();
        }
        foreach ($this->indentSubscriptions as $item) {
            if (!($item instanceof IndentSubscription)) {
                throw  new \InvalidArgumentException();
            }
            $this->total += $item->getPrice();
        }
        if ($this->getShipping()) {
            $this->total += $this->getShipping()->getPrice();
        }

        return $this->total;
    }

    /**
     * Set number.
     *
     * @param  string  $number
     *
     * @return Indent
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set disableShipping.
     *
     * @param  bool|null  $disableShipping
     *
     * @return Indent
     */
    public function setDisableShipping($disableShipping = null)
    {
        $this->disableShipping = $disableShipping;

        return $this;
    }

    /**
     * Get disableShipping.
     *
     * @return bool|null
     */
    public function getDisableShipping()
    {
        return $this->disableShipping;
    }

    /**
     * Add indentSubscription.
     *
     * @param  \AppBundle\Entity\IndentSubscription  $indentSubscription
     *
     * @return Indent
     */
    public function addIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        $this->indentSubscriptions[] = $indentSubscription;

        return $this;
    }

    /**
     * Remove indentSubscription.
     *
     * @param  \AppBundle\Entity\IndentSubscription  $indentSubscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        return $this->indentSubscriptions->removeElement($indentSubscription);
    }

    /**
     * Get indentSubscriptions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndentSubscriptions()
    {
        return $this->indentSubscriptions;
    }


    /**
     * Set tempEnded.
     *
     * @param  bool|null  $tempEnded
     *
     * @return Indent
     */
    public function setTempEnded($tempEnded = null)
    {
        $this->tempEnded = $tempEnded;

        return $this;
    }

    /**
     * Get tempEnded.
     *
     * @return bool|null
     */
    public function getTempEnded()
    {
        return $this->tempEnded;
    }
}
