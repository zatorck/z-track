<?php


namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\Part\Base;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 *
 * @package AppBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity("email")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{

    use Base;

    use TraccarFields;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="Indent", mappedBy="user")
     */
    private $indents;

    /**
     * @ORM\OneToMany(targetEntity="Log", mappedBy="user")
     */
    private $logs;

    /**
     * @ORM\ManyToMany(targetEntity="Device", mappedBy="users")
     */
    private $devices;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DeviceHistory", mappedBy="user")
     */
    private $deviceHistories;


    /**
     * @var boolean
     */
    private $dontUseTraccarApi = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $traccarAdministrator = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $traccarReadonly = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $invoice;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $traccarDeviceReadonly = true;
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $traccarToken;


    /**
     * @Assert\Valid(groups={"user"})
     * @ORM\OneToOne(targetEntity="Address", cascade={"all"})
     * @ORM\JoinColumn(name="invoice_address_id", referencedColumnName="id", nullable=true)
     */
    private $invoiceAddress;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    private $changingPassword;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->features = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->deviceHistories = new ArrayCollection();
        $this->logs = new ArrayCollection();

        parent::__construct();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->email;
    }

    /**
     * @param $email
     *
     * @return User
     */
    public function setEmail($email): self
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }

    /**
     * @param  Indent  $indent
     *
     * @return User
     */
    public function addIndent(Indent $indent): self
    {
        $this->indents[] = $indent;

        return $this;
    }

    /**
     * @param  Indent  $indent
     *
     * @return bool
     */
    public function removeIndent(Indent $indent): bool
    {
        return $this->indents->removeElement($indent);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndents(): Collection
    {
        return $this->indents;
    }

    /**
     * Add device.
     *
     * @param  \AppBundle\Entity\Device  $device
     *
     * @return User
     */
    public function addDevice(Device $device): self
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device.
     *
     * @param  \AppBundle\Entity\Device  $device
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDevice(Device $device): bool
    {
        return $this->devices->removeElement($device);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    /**
     * Add deviceHistory.
     *
     * @param  \AppBundle\Entity\DeviceHistory  $deviceHistory
     *
     * @return User
     */
    public function addDeviceHistory(DeviceHistory $deviceHistory): self
    {
        $this->deviceHistories[] = $deviceHistory;

        return $this;
    }

    /**
     * Remove deviceHistory.
     *
     * @param  \AppBundle\Entity\DeviceHistory  $deviceHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDeviceHistory(DeviceHistory $deviceHistory): bool
    {
        return $this->deviceHistories->removeElement($deviceHistory);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeviceHistories(): Collection
    {
        return $this->deviceHistories;
    }


    /**
     * Add log.
     *
     * @param  \AppBundle\Entity\Log  $log
     *
     * @return User
     */
    public function addLog(\AppBundle\Entity\Log $log)
    {
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log.
     *
     * @param  \AppBundle\Entity\Log  $log
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLog(\AppBundle\Entity\Log $log)
    {
        return $this->logs->removeElement($log);
    }

    /**
     * Get logs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * Set traccarAdministrator.
     *
     * @param  bool|null  $traccarAdministrator
     *
     * @return User
     */
    public function setTraccarAdministrator($traccarAdministrator = null)
    {
        $this->traccarAdministrator = $traccarAdministrator;

        return $this;
    }

    /**
     * Get traccarAdministrator.
     *
     * @return bool|null
     */
    public function getTraccarAdministrator()
    {
        return $this->traccarAdministrator;
    }

    /**
     * Set traccarReadonly.
     *
     * @param  bool|null  $traccarReadonly
     *
     * @return User
     */
    public function setTraccarReadonly($traccarReadonly = null)
    {
        $this->traccarReadonly = $traccarReadonly;

        return $this;
    }

    /**
     * Get traccarReadonly.
     *
     * @return bool|null
     */
    public function getTraccarReadonly()
    {
        return $this->traccarReadonly;
    }

    /**
     * Set traccarDeviceReadonly.
     *
     * @param  bool|null  $traccarDeviceReadonly
     *
     * @return User
     */
    public function setTraccarDeviceReadonly($traccarDeviceReadonly = null)
    {
        $this->traccarDeviceReadonly = $traccarDeviceReadonly;

        return $this;
    }

    /**
     * Get traccarDeviceReadonly.
     *
     * @return bool|null
     */
    public function getTraccarDeviceReadonly()
    {
        return $this->traccarDeviceReadonly;
    }

    /**
     * @return bool
     */
    public function isDontUseTraccarApi(): bool
    {
        return $this->dontUseTraccarApi;
    }

    /**
     * @param  bool  $dontUseTraccarApi
     *
     * @return User
     */
    public function setDontUseTraccarApi(bool $dontUseTraccarApi): User
    {
        $this->dontUseTraccarApi = $dontUseTraccarApi;

        return $this;
    }

    /**
     * Set traccarToken.
     *
     * @param  string|null  $traccarToken
     *
     * @return User
     */
    public function setTraccarToken($traccarToken = null)
    {
        $this->traccarToken = $traccarToken;

        return $this;
    }

    /**
     * Get traccarToken.
     *
     * @return string|null
     */
    public function getTraccarToken()
    {
        return $this->traccarToken;
    }


    /**
     * Set invoiceAddress.
     *
     * @param  \AppBundle\Entity\Address|null  $invoiceAddress
     *
     * @return User
     */
    public function setInvoiceAddress(?\AppBundle\Entity\Address $invoiceAddress = null)
    {
        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * Get invoiceAddress.
     *
     * @return \AppBundle\Entity\Address|null
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * Set invoice.
     *
     * @param  bool|null  $invoice
     *
     * @return User
     */
    public function setInvoice($invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice.
     *
     * @return bool|null
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set nip.
     *
     * @param  string|null  $nip
     *
     * @return User
     */
    public function setNip($nip = null)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip.
     *
     * @return string|null
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Set phoneNumber.
     *
     * @param  string|null  $phoneNumber
     *
     * @return User
     */
    public function setPhoneNumber($phoneNumber = null)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber.
     *
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        $this->changingPassword = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChangingPassword()
    {
        return $this->changingPassword;
    }

}
