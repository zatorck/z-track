<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Shipping
 *
 * @package AppBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="shipping")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShippingRepository")
 */
class Shipping
{
    use Base;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $cashOnDelivery;

    /**
     * @ORM\ManyToMany(targetEntity="Country", inversedBy="shippings")
     * @ORM\JoinTable(name="countires_shippings")
     */
    private $countries;

    /**
     * Shipping constructor.
     */
    public function __construct()
    {
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name.' ('.number_format($this->price, 2, ',', ' ').' zł)';
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Shipping
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Shipping
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Shipping
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param Country $country
     *
     * @return Shipping
     */
    public function addCountry(Country $country): self
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * @param Country $country
     *
     * @return bool
     */
    public function removeCountry(Country $country): bool
    {
        return $this->countries->removeElement($country);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries(): \Doctrine\Common\Collections\Collection
    {
        return $this->countries;
    }

    /**
     * @return bool
     */
    public function getCashOnDelivery(): ?bool
    {
        return $this->cashOnDelivery;
    }

    /**
     * @param bool|null $cashOnDelivery
     *
     * @return Shipping
     */
    public function setCashOnDelivery(?bool $cashOnDelivery = null): self
    {
        $this->cashOnDelivery = $cashOnDelivery;

        return $this;
    }
}
