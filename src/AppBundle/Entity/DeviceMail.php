<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * DeviceMail
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="device_mail")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeviceMailRepository")
 */
class DeviceMail
{

    use Base;

    /**
     * @var int
     *
     * @ORM\Column(name="daysLeft", type="integer")
     */
    private $daysLeft;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Device", inversedBy="deviceMails")
     * @ORM\JoinColumn(name="device", referencedColumnName="id", onDelete="CASCADE")
     */
    private $device;

    /**
     * Set daysLeft.
     *
     * @param  int  $daysLeft
     *
     * @return DeviceMail
     */
    public function setDaysLeft($daysLeft)
    {
        $this->daysLeft = $daysLeft;

        return $this;
    }

    /**
     * Get daysLeft.
     *
     * @return int
     */
    public function getDaysLeft()
    {
        return $this->daysLeft;
    }

    /**
     * Set device.
     *
     * @param \AppBundle\Entity\Device|null $device
     *
     * @return DeviceMail
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device.
     *
     * @return \AppBundle\Entity\Device|null
     */
    public function getDevice()
    {
        return $this->device;
    }
}
