<?php



namespace AppBundle\Entity\Part;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait Metatags
 *
 * @package AppBundle\Entity\Part
 */
trait Metatags
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     *
     * @return Metatags
     */
    public function setMetaDescription(?string $metaDescription = null): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string|null $metaTitle
     *
     * @return Metatags
     */
    public function setMetaTitle(?string $metaTitle = null): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }
}
