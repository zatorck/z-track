<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Subscription
 *
 * @package AppBundle\Entity
 *
 * @Vich\Uploadable
 * @UniqueEntity("months")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionRepository")
 */
class Subscription
{

    use Base;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", unique=true)
     */
    private $months;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Vich\UploadableField(mapping="subscription_image", fileNameProperty="fileName")
     *
     * @var File
     */
    private $file;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $fileName;


    /**
     * @ORM\OneToMany(targetEntity="IndentSubscription", mappedBy="subscription", cascade={"all"})
     */
    private $indentSubscriptions;

    /**
     * Indent constructor.
     */
    public function __construct()
    {
        $this->indentSubscriptions = new ArrayCollection();
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param  File|null  $image
     *
     * @return Subscription
     * @throws \Exception
     */
    public function setFile(File $image = null): self
    {

        $this->file = $image;

        if (null !== $image) {
            $this->fileAdded = true;
            $this->updatedAt = new \DateTime();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param  string  $fileName
     *
     * @return Subscription
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMonths(): ?int
    {
        return $this->months;
    }

    /**
     * @param  int  $months
     *
     * @return Subscription
     */
    public function setMonths(int $months): self
    {
        $this->months = $months;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param  float  $price
     *
     * @return Subscription
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param  bool  $active
     *
     * @return Subscription
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }


    /**
     * Add indentSubscription.
     *
     * @param \AppBundle\Entity\IndentSubscription $indentSubscription
     *
     * @return Subscription
     */
    public function addIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        $this->indentSubscriptions[] = $indentSubscription;

        return $this;
    }

    /**
     * Remove indentSubscription.
     *
     * @param \AppBundle\Entity\IndentSubscription $indentSubscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        return $this->indentSubscriptions->removeElement($indentSubscription);
    }

    /**
     * Get indentSubscriptions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndentSubscriptions()
    {
        return $this->indentSubscriptions;
    }
}
