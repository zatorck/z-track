<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DeviceHistory
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="deviceHistory")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeviceHistoryRepository")
 */
class DeviceHistory
{
    use Base;


    /**
     * @var \DateTime|null
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fromEndAt;


    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @ORM\Column(type="datetime")
     */
    private $toEndAt;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $daysDiff;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="deviceHistories")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="SET NULL")
     */
    private $user;

    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="deviceHistories")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $device;

    /**
     * Set fromEndAt.
     *
     * @param  \DateTime  $fromEndAt
     *
     * @return DeviceHistory
     */
    public function setFromEndAt($fromEndAt)
    {
        $this->fromEndAt = $fromEndAt;

        return $this;
    }

    /**
     * Get fromEndAt.
     *
     * @return \DateTime
     */
    public function getFromEndAt()
    {
        return $this->fromEndAt;
    }


    /**
     * Set toEndAt.
     *
     * @param  \DateTime  $toEndAt
     *
     * @return DeviceHistory
     */
    public function setToEndAt($toEndAt)
    {
        $this->toEndAt = $toEndAt;

        return $this;
    }

    /**
     * Get toEndAt.
     *
     * @return \DateTime
     */
    public function getToEndAt()
    {
        return $this->toEndAt;
    }

    /**
     * Set daysDiff.
     *
     * @param  int|null  $daysDiff
     *
     * @return DeviceHistory
     */
    public function setDaysDiff(?int $daysDiff)
    {
        $this->daysDiff = $daysDiff;

        return $this;
    }

    /**
     * Get daysDiff.
     *
     * @return int|null
     */
    public function getDaysDiff(): ?int
    {
        return $this->daysDiff;
    }

    /**
     * Set user.
     *
     * @param  \AppBundle\Entity\User|null  $user
     *
     * @return DeviceHistory
     */
    public function setUser(User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set device.
     *
     * @param  \AppBundle\Entity\Device|null  $device
     *
     * @return DeviceHistory
     */
    public function setDevice(Device $device = null): self
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device.
     *
     * @return \AppBundle\Entity\Device|null
     */
    public function getDevice(): ?Device
    {
        return $this->device;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function calculateDaysDiff()
    {
        $earlier = $this->getFromEndAt() ? $this->getFromEndAt() : new \DateTime();

        $later = $this->getToEndAt();

        $this->daysDiff = $later->diff($earlier)->format("%a");
    }
}
