<?php


namespace AppBundle\Entity;

use AppBundle\AppSettings;
use AppBundle\Entity\Part\Base;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Device
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="device")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeviceRepository")
 * @UniqueEntity("serialNumber")
 */
class Device
{
    use Base;
    use TraccarFields;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $serialNumber;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @var string
     *
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $simNumber;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $simEndAt;


    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notice;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="devices")
     * @ORM\JoinTable(name="users_devices")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DeviceHistory", mappedBy="device")
     */
    private $deviceHistories;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $disabledInTraccar;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DeviceMail", mappedBy="device")
     */
    private $deviceMails;

    /**
     * @var Collection|null
     */
    private $userChangeSet;

    /**
     * @var boolean
     */
    private $changedDisabledInTraccar = false;


    /**
     * @ORM\OneToMany(targetEntity="IndentSubscription", mappedBy="device", cascade={"all"})
     */
    private $indentSubscriptions;

    /**
     * @var boolean
     */
    private $dontUseTraccarApi = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $configured;

    /**
     * Device constructor.
     */
    public function __construct()
    {
        $this->deviceHistories = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->deviceMails = new ArrayCollection();
        $this->indentSubscriptions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    /**
     * @param  string  $serialNumber
     *
     * @return Country
     */
    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param  string  $name
     *
     * @return Country
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set endAt.
     *
     * @param  \DateTime  $endAt
     *
     * @return Device
     */
    public function setEndAt($endAt)
    {
        $endAt->modify('+23 hours');
        $endAt->modify('+59 minutes');
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getEndAt()
    {

        return $this->endAt ? $this->endAt : new \DateTime(AppSettings::DEFAULT_FREE_END_AT);
    }


    /**
     * Add deviceHistory.
     *
     * @param  \AppBundle\Entity\DeviceHistory  $deviceHistory
     *
     * @return User
     */
    public function addDeviceHistory(DeviceHistory $deviceHistory): self
    {
        $this->deviceHistories[] = $deviceHistory;

        return $this;
    }

    /**
     * Remove deviceHistory.
     *
     * @param  \AppBundle\Entity\DeviceHistory  $deviceHistory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDeviceHistory(DeviceHistory $deviceHistory): bool
    {
        return $this->deviceHistories->removeElement($deviceHistory);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeviceHistories(): Collection
    {
        return $this->deviceHistories;
    }

    /**
     * @return string|null
     */
    public function getNotice(): ?string
    {
        return $this->notice;
    }

    /**
     * @param  string|null  $notice
     *
     * @return Product
     */
    public function setNotice(?string $notice = null): self
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Set disabledInTraccar.
     *
     * @param  bool  $disabledInTraccar
     *
     * @return Device
     */
    public function setDisabledInTraccar($disabledInTraccar)
    {
        if (!$this->disabledInTraccar && $this->disabledInTraccar != $disabledInTraccar) {
            $this->changedDisabledInTraccar = true;
        }
        $this->disabledInTraccar = $disabledInTraccar;

        return $this;
    }

    /**
     * Get disabledInTraccar.
     *
     * @return bool
     */
    public function getDisabledInTraccar()
    {
        return $this->disabledInTraccar;
    }

    /**
     * Add user.
     *
     * @param  \AppBundle\Entity\User  $user
     *
     * @return Device
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param  \AppBundle\Entity\User  $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        return $this->users->removeElement($user);
    }

    public function hasUser(User $user)
    {
        foreach ($this->users as $deviceUser) {
            if ($user->getId() == $deviceUser->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getUserChangeSet()
    {
        return $this->userChangeSet;
    }

    /**
     * @ORM\PostLoad()
     */
    public function setUserChangeSet()
    {
        $this->userChangeSet = clone $this->users;
    }

    /**
     * Get users.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }


    /**
     * @return bool
     */
    public function getChangedDisabledInTraccar()
    {
        return $this->changedDisabledInTraccar;
    }

    /**
     * Add deviceMail.
     *
     * @param  \AppBundle\Entity\DeviceMail  $deviceMail
     *
     * @return Device
     */
    public function addDeviceMail(\AppBundle\Entity\DeviceMail $deviceMail)
    {
        $this->deviceMails[] = $deviceMail;

        return $this;
    }

    /**
     * Remove deviceMail.
     *
     * @param  \AppBundle\Entity\DeviceMail  $deviceMail
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDeviceMail(\AppBundle\Entity\DeviceMail $deviceMail)
    {
        return $this->deviceMails->removeElement($deviceMail);
    }

    /**
     * Get deviceMails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeviceMails()
    {
        return $this->deviceMails;
    }

    /**
     * Add indentSubscription.
     *
     * @param  \AppBundle\Entity\IndentSubscription  $indentSubscription
     *
     * @return Device
     */
    public function addIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        $this->indentSubscriptions[] = $indentSubscription;

        return $this;
    }

    /**
     * Remove indentSubscription.
     *
     * @param  \AppBundle\Entity\IndentSubscription  $indentSubscription
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIndentSubscription(\AppBundle\Entity\IndentSubscription $indentSubscription)
    {
        return $this->indentSubscriptions->removeElement($indentSubscription);
    }

    /**
     * Get indentSubscriptions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndentSubscriptions()
    {
        return $this->indentSubscriptions;
    }

    /**
     * Set simNumber.
     *
     * @param  string|null  $simNumber
     *
     * @return Device
     */
    public function setSimNumber($simNumber = null)
    {
        $this->simNumber = $simNumber;

        return $this;
    }

    /**
     * Get simNumber.
     *
     * @return string|null
     */
    public function getSimNumber()
    {
        return $this->simNumber;
    }

    /**
     * Set simEndAt.
     *
     * @param  \DateTime|null  $simEndAt
     *
     * @return Device
     */
    public function setSimEndAt($simEndAt = null)
    {
        $this->simEndAt = $simEndAt;

        return $this;
    }

    /**
     * Get simEndAt.
     *
     * @return \DateTime|null
     */
    public function getSimEndAt()
    {
        return $this->simEndAt;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getSimEndAtClass()
    {
        $now = new \DateTime();
        $date = new \DateTime('+7 days');

        if ($this->simEndAt < $now) {
            return 'text-danger';
        }

        if ($this->simEndAt < $date) {
            return 'text-warning';
        }

        return 'text-success';
    }


    /**
     * @return bool
     */
    public function isDontUseTraccarApi(): bool
    {
        return $this->dontUseTraccarApi;
    }

    /**
     * @param  bool  $dontUseTraccarApi
     *
     * @return Device
     */
    public function setDontUseTraccarApi(bool $dontUseTraccarApi): Device
    {
        $this->dontUseTraccarApi = $dontUseTraccarApi;

        return $this;
    }

    /**
     * Set configured.
     *
     * @param bool|null $configured
     *
     * @return Device
     */
    public function setConfigured($configured = null)
    {
        $this->configured = $configured;

        return $this;
    }

    /**
     * Get configured.
     *
     * @return bool|null
     */
    public function getConfigured()
    {
        return $this->configured;
    }
}
