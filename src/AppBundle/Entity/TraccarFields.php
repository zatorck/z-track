<?php


namespace AppBundle\Entity;


/**
 * Trait TraccarFields
 *
 * @package AppBundle\Entity
 */
trait TraccarFields
{
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true, unique=true)
     */
    private $traccarId;

    /**
     * @return int|null
     */
    public function getTraccarId(): ?int
    {
        return $this->traccarId;
    }

    /**
     * @param  int|null  $traccarId
     *
     * @return self
     */
    public function setTraccarId(?int $traccarId): self
    {
        $this->traccarId = $traccarId;

        return $this;
    }


}
