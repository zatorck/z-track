<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * IndentSubscription
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="indent_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IndentSubscriptionRepository")
 */
class IndentSubscription
{
    use Base;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $months;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="Subscription", inversedBy="indentSubscriptions")
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $subscription;

    /**
     * @ORM\ManyToOne(targetEntity="Indent", inversedBy="indentSubscriptions", cascade={"persist"})
     * @ORM\JoinColumn(name="indent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $indent;

    /**
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="indentSubscriptions", cascade={"persist"})
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $device;



    /**
     * @return int|null
     */
    public function getMonths(): ?int
    {
        return $this->months;
    }

    /**
     * @param  int  $months
     *
     * @return Subscription
     */
    public function setMonths(int $months): self
    {
        $this->months = $months;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param  float  $price
     *
     * @return Subscription
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Set subscription.
     *
     * @param \AppBundle\Entity\Subscription|null $subscription
     *
     * @return IndentSubscription
     */
    public function setSubscription(\AppBundle\Entity\Subscription $subscription = null)
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * Get subscription.
     *
     * @return \AppBundle\Entity\Subscription|null
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * Set indent.
     *
     * @param \AppBundle\Entity\Indent|null $indent
     *
     * @return IndentSubscription
     */
    public function setIndent(\AppBundle\Entity\Indent $indent = null)
    {
        $this->indent = $indent;

        return $this;
    }

    /**
     * Get indent.
     *
     * @return \AppBundle\Entity\Indent|null
     */
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * Set device.
     *
     * @param \AppBundle\Entity\Device|null $device
     *
     * @return IndentSubscription
     */
    public function setDevice(\AppBundle\Entity\Device $device = null)
    {
        $this->device = $device;

        return $this;
    }

    /**
     * Get device.
     *
     * @return \AppBundle\Entity\Device|null
     */
    public function getDevice()
    {
        return $this->device;
    }
}
