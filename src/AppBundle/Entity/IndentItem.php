<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class IndentItem
 *
 * @package AppBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="indent_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IndentItemRepository")
 */
class IndentItem
{
    use Base;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="Indent", inversedBy="indentItems", cascade={"persist"})
     * @ORM\JoinColumn(name="indent_it", referencedColumnName="id", onDelete="CASCADE")
     */
    private $indent;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return IndentItem
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return IndentItem
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return IndentItem
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     *
     * @return IndentItem
     */
    public function setProduct(?Product $product = null): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Indent|null
     */
    public function getIndent(): ?Indent
    {
        return $this->indent;
    }

    /**
     * @param Indent $indent
     *
     * @return IndentItem
     */
    public function setIndent(Indent $indent): self
    {
        $this->indent = $indent;

        return $this;
    }
}
