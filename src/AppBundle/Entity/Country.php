<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country
 *
 * @package AppBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 */
class Country
{
    use Base;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Shipping", mappedBy="countries")
     */
    private $shippings;

    /**
     * Country constructor.
     */
    public function __construct()
    {
        $this->shippings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->name;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Country
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Shipping $shipping
     *
     * @return Country
     */
    public function addShipping(Shipping $shipping): self
    {
        $this->shippings[] = $shipping;

        return $this;
    }

    /**
     * @param Shipping $shipping
     */
    public function removeShipping(Shipping $shipping): void
    {
        $this->shippings->removeElement($shipping);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippings(): \Doctrine\Common\Collections\Collection
    {
        return $this->shippings;
    }
}
