<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Image
 *
 * @package AppBundle\Entity
 *
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 */
class Image
{

    use Base;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\Expression("this.getFile() or this.getFileAdded()", message="Dodaj zdjęcie")
     * @Vich\UploadableField(mapping="image", fileNameProperty="name")
     *
     * @var File
     */
    private $file;

    /**
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @var boolean
     */
    private $fileAdded;

    /**
     * @return File
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $image
     *
     * @throws \Exception
     */
    public function setFile(?File $image = null): void
    {

        $this->file = $image;

        if (null !== $image) {
            $this->fileAdded = true;
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Image
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Image
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFileAdded(): ?bool
    {
        return $this->fileAdded;
    }

}
