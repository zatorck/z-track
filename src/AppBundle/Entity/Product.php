<?php



namespace AppBundle\Entity;

use AppBundle\Entity\Part\Base;
use AppBundle\Entity\Part\Metatags;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product
 *
 * @package AppBundle\Entity
 *
 * @UniqueEntity("slug")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{

    use Base;
    use Metatags;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $shortContent;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $testimonialContent;

    /**
     * @var float
     * @Assert\NotBlank()
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @Assert\Valid()
     *
     * @ORM\OrderBy({"position" = "asc"})
     * @ORM\ManyToMany(targetEntity="Image", cascade={"all"})
     * @ORM\JoinTable(name="products_images",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    private $images;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getTitle();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Product
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Product
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return Product
     */
    public function setContent(?string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return Product
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     *
     * @return Product
     */
    public function setActive(?bool $active = null): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return Product
     */
    public function addImage(Image $image): self
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * @param Image $image
     *
     * @return bool
     */
    public function removeImage(Image $image): bool
    {
        return $this->images->removeElement($image);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages(): \Doctrine\Common\Collections\Collection
    {
        return $this->images;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     *
     * @return Product
     */
    public function setCategory(?Category $category = null): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortContent(): ?string
    {
        return $this->shortContent;
    }

    /**
     * @param string|null $shortContent
     *
     * @return Product
     */
    public function setShortContent(?string $shortContent = null): self
    {
        $this->shortContent = $shortContent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTestimonialContent(): ?string
    {
        return $this->testimonialContent;
    }

    /**
     * @param string|null $testimonialContent
     *
     * @return Product
     */
    public function setTestimonialContent(?string $testimonialContent = null): self
    {
        $this->testimonialContent = $testimonialContent;

        return $this;
    }

}
