<?php



namespace AppBundle\Fetcher;

use AppBundle\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ProductFetcher
 *
 * @package AppBundle\Fetcher
 */
class ProductFetcher implements FetcherInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductFetcher constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function fetchProductsFromArrayOfIds(array $ids): array
    {
        return $this->getRepository()->getProductsFromArrayOfIdsQuery($ids)
            ->getResult();
    }

    /**
     * @return ProductRepository
     */
    public function getRepository(): ProductRepository
    {
        return $this->em->getRepository('AppBundle:Product');
    }

}
