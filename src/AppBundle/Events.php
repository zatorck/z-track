<?php

namespace AppBundle;

/**
 * Class Events
 *
 * @package AppBundle
 */
class Events
{
    const INDENT_CREATE = 'indent.create';
    const USER_CREATE = 'user.create';
}