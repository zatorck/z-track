<?php



namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Device;
use AppBundle\Entity\IndentSubscription;
use AppBundle\Entity\Shipping;
use AppBundle\Entity\Subscription;
use AppBundle\Utils\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class IndentSubscriptionType
 *
 * @package AppBundle\Form
 */
class IndentSubscriptionType extends AbstractType
{

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $indentSubscriptions;

    /**
     * @var \AppBundle\Entity\User|null
     */
    private $currentUser;

    private $subscriptions;

    /**
     * IndentSubscriptionType constructor.
     *
     */
    public function __construct(CurrentUser $currentUser, EntityManagerInterface $em)
    {
        $currentUser = $currentUser->getCurrentUser();
        $this->currentUser = $currentUser;
        $this->indentSubscriptions = [];

        foreach ($currentUser->getDevices() as $device) {
            $indentSubscription = new IndentSubscription();
            $indentSubscription->setDevice($device);
            $this->indentSubscriptions[] = $indentSubscription;
        }

        $this->subscriptions = $em->getRepository('AppBundle:Subscription')->findAllActive();

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('indentSubscriptions', ChoiceType::class, [
                                'multiple' => true,
                                'expanded' => true,
                                'choices' => $this->indentSubscriptions,
                                'choice_label' => function (IndentSubscription $indentSubscription, $key, $value) {
                                    return ' ';
                                },
                                'data' => $this->indentSubscriptions,
                        ]
                )
                ->add('subscription', ChoiceType::class, [
                                'mapped' => false,
                                'choices' => $this->subscriptions,
                                'data' => $this->subscriptions,
                                'label' => false,
                                'choice_label' => function (Subscription $subscription, $key, $value) {
                                    return $subscription->getMonths().' miesięc(y) - ('.$subscription->getPrice().' PLN / urządzenie)';
                                },
                        ]
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
                array(
                        'data_class' => 'AppBundle\Entity\Indent',
                        'validation_groups' => function (FormInterface $form) {
                            $data = $form->getData();

                            $groups = ['Default'];


                            return $groups;
                        },
                )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_indent';
    }

}
