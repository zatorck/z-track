<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nazwa'])
            ->add('state', TextType::class, ['label' => 'Województwo'])
            ->add('street', TextType::class, ['label' => 'Ulica'])
            ->add('postCode', TextType::class, ['label' => 'Kod pocztowy'])
            ->add('city', TextType::class, ['label' => 'Miasto'])
            ->add('nip', TextType::class, ['label' => 'NIP'])
            ->add('slug', TextType::class, ['label' => 'Kóncówka url'])
            ->add('phone', TextType::class, ['label' => 'Tel'])
            ->add('lat', TextType::class, ['label' => 'Szerokość geo na mapę'])
            ->add('lon', TextType::class, ['label' => 'Długość geo na mapę'])
            ->add('email', TextType::class, ['label' => 'Email'])
            ->add('openingHours', TextareaType::class, ['label' => 'Godziny otwarcia'])
            ->add('description', TextareaType::class, ['label' => 'Opis'])
            ->add('metaDescription', TextareaType::class, ['label' => 'Metatag opis'])
            ->add('metaTitle', TextareaType::class, ['label' => 'Metatag tytuł'])
            ->add('active', CheckboxType::class, ['label' => 'Czy aktywny']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Partner'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_partner';
    }


}
