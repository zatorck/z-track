<?php


namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Shipping;
use AppBundle\Utils\CurrentUser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class IndentType
 *
 * @package AppBundle\Form
 */
class IndentType extends AbstractType
{

    private $user;

    public function __construct(CurrentUser $currentUser)
    {
        $this->user = $currentUser->getCurrentUser();
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add(
                        'invoice',
                        ChoiceType::class,
                        [
                                'label' => false,
                                'choices' => [
                                        'Nie' => false,
                                        'Tak' => true,
                                ],
                                'data' => $this->user ?  $this->user->getInvoice() : false,
                        ]
                )
                ->add(
                        'nip',
                        TextType::class,
                        [
                                'label' => 'NIP',
                                'required' => false,
                                'data' => $this->user ?  $this->user->getNip() : false,
                        ]
                )
                ->add(
                        'phoneNumber',
                        TextType::class,
                        [
                                'label' => 'Numer telefonu',
                                'data' => $this->user ?  $this->user->getPhoneNumber() : false,
                        ]
                )
                ->add(
                        'email',
                        EmailType::class,
                        [
                                'label' => 'Adres Email',
                        ]
                )
                ->add(
                        'shippingAddress',
                        AddressType::class,
                        [
                                'label' => false,
                        ]
                )
                ->add(
                        'invoiceAddress',
                        AddressType::class,
                        [
                                'label' => false,
                                'required' => false,
                                'data' =>  ($this->user && $this->user->getInvoiceAddress()) ? clone $this->user->getInvoiceAddress() : null,
                        ]
                )
                ->add(
                        'shipping',
                        EntityType::class,
                        [
                                'label' => false,
                                'class' => Shipping::class,
                        ]
                )
                ->add(
                        'country',
                        EntityType::class,
                        [
                                'label' => 'Kraj',
                                'class' => Country::class,

                        ]
                )
                ->add(
                        'info',
                        TextareaType::class,
                        [
                                'label' => 'Dodatkowe info',
                                'attr' => [
                                        'placeholder' => 'Maks 1020 znaków',
                                ],
                                'required' => false,
                        ]
                )
                ->add(
                        'paczkomat',
                        TextType::class,
                        [
                                'label' => 'Wybrany paczkomat',
                                'attr' => [
                                        'placeholder' => 'kliknij aby wybrać paczkomat',
                                        'class' => ' js-message-popup ',
                                ],
                                'required' => false,
                        ]
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
                array(
                        'data_class' => 'AppBundle\Entity\Indent',
                        'validation_groups' => function (FormInterface $form) {
                            $data = $form->getData();

                            $groups = ['Default', 'items', 'phone'];

                            if ($data->getInvoice()) {
                                $groups [] = 'invoice';
                            }

                            if ($data->getShipping()->getType() == 'inpost-paczkomaty') {
                                $groups[] = 'paczkomat';
                            }

                            return $groups;
                        },
                )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_indent';
    }

}
