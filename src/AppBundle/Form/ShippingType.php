<?php



namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Countryl;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShippingType
 *
 * @package AppBundle\Form
 */
class ShippingType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Nazwa',
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'label' => 'Typ specjalny',
                    'choices' => [
                        'Brak typu specjalnego' => 'normal',
                        'Inpost (paczkomat zamiast adresu)' => 'inpost-paczkomaty',
                    ],
                ]
            )
            ->add(
                'cashOnDelivery',
                ChoiceType::class,
                [
                    'label' => 'Płatność przy odbiorze',
                    'choices' => [
                        'Tak' => true,
                        'Nie' => false,
                    ],
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'label' => 'Cena',
                    'currency' => 'PLN',
                ]
            )
            ->add(
                'countries',
                EntityType::class,
                [
                    'label' => 'Państwa',
                    'class' => Country::class,
                    'expanded' => true,
                    'multiple' => true,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Shipping',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_shipping';
    }

}
