<?php



namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * Class SubscriptionType
 *
 * @package AppBundle\Form
 */
class SubscriptionType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'months',
                IntegerType::class,
                [
                    'label' => 'Liczba miesięcy',
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'label' => 'Cena',
                    'currency' => 'PLN',
                ]
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label' => 'Pokaż na stronie',
                    'choices' => [
                        'Tak' => true,
                        'Nie' => false,
                    ],
                ]
            )
            ->add(
                'file',
                VichImageType::class,
                [
                    'required' => false,
                    'label' => 'Obrazek',
                    'allow_delete' => true,
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Subscription',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_subscription';
    }

}
