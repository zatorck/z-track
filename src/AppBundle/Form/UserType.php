<?php



namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * Class RegistrationType
 *
 * @package AppBundle\Form
 */
class UserType extends AbstractType
{

    /**
     * @param  FormBuilderInterface  $builder
     * @param  array  $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->remove('username')
                ->add('sendEmail', CheckboxType::class, [
                        'mapped' => false,
                        'label' => 'Powiadom  użytkownika - wyślij mu maila',
                        'required' => false,
                ])
                ->add('traccarToken')
                ->add(
                        'roles',
                        ChoiceType::class,
                        array(
                                'required' => true,
                                'choices' => array('Admin aplikacji' => 'ROLE_ADMIN'),
                                'multiple' => true,
                                'expanded' => true,
                                'label' => false,
                                'label_attr' => array('class' => 'checkbox-inline'),
                        )
                )

                ->add('traccarAdministrator', CheckboxType::class, [
                        'label' => 'Traccar panel - Admin',
                        'required' => false,
                ])
                ->add('traccarReadonly', CheckboxType::class, [
                        'label' => 'Traccar panel - Tylko do odczytu',
                        'required' => false,
                ])
                ->add('traccarDeviceReadonly', CheckboxType::class, [
                        'label' => 'Traccar panel - Urządzenia tylko do odczytu',
                        'required' => false,
                ]);
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}
