<?php



namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CategoryType
 *
 * @package AppBundle\Form
 */
class CategoryType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'Nazwa w menu',
                ]
            )
            ->add(
                'slug',
                TextType::class,
                [
                    'label' => 'Końcówka Url',
                ]
            )
            ->add(
                'position',
                IntegerType::class,
                [
                    'label' => 'Priorytet wyświetlania',
                ]
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label' => 'Pokaż na stronie',
                    'choices' => array(
                        'Tak' => true,
                        'Nie' => false,
                    ),
                ]
            )
            ->add(
                'metaDescription',
                TextareaType::class,
                [
                    'label' => 'Metatag - Opis',
                    'attr' => [
                        'rows' => 5,
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'metaTitle',
                TextareaType::class,
                [
                    'label' => 'Metatag - Tytuł',
                    'attr' => [
                        'rows' => 5,
                    ],
                    'required' => false,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Category',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_category';
    }

}
