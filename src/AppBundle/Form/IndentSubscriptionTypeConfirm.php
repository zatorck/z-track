<?php


namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Device;
use AppBundle\Entity\IndentSubscription;
use AppBundle\Entity\Shipping;
use AppBundle\Entity\Subscription;
use AppBundle\Utils\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class IndentSubscriptionTypeConfirm
 *
 * @package AppBundle\Form
 */
class IndentSubscriptionTypeConfirm extends AbstractType
{

    private $user;

    public function __construct(CurrentUser $currentUser)
    {
        $this->user = $currentUser->getCurrentUser();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add(
                        'invoice',
                        ChoiceType::class,
                        [
                                'label' => false,
                                'choices' => [
                                        'Nie' => false,
                                        'Tak' => true,
                                ],
                                'data' => $this->user ?  $this->user->getInvoice() : false,
                        ]
                )
                ->add(
                        'nip',
                        TextType::class,
                        [
                                'label' => 'NIP',
                                'required' => false,
                                'data' => $this->user ?  $this->user->getNip() : false,

                        ]
                )
                ->add(
                        'invoiceAddress',
                        AddressType::class,
                        [
                                'label' => false,
                                'required' => false,
                                'data' => ($this->user && $this->user->getInvoiceAddress()) ? clone $this->user->getInvoiceAddress() : null,
                        ]
                )
                ->add(
                        'info',
                        TextareaType::class,
                        [
                                'label' => 'Dodatkowe info',
                                'attr' => [
                                        'placeholder' => 'Maks 1020 znaków',
                                ],
                                'required' => false,
                        ]
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
                array(
                        'data_class' => 'AppBundle\Entity\Indent',
                        'validation_groups' => function (FormInterface $form) {
                            $data = $form->getData();

                            $groups = ['Default', 'subscription'];

                            if ($data->getInvoice()) {
                                $groups [] = 'invoice';
                            }


                            return $groups;
                        },
                )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_indent';
    }

}
