<?php



namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType
 *
 * @package AppBundle\Form
 */
class ProductType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'Nazwa w menu',
                ]
            )
            ->add(
                'slug',
                TextType::class,
                [
                    'label' => 'Końcówka Url',
                ]
            )
            ->add(
                'position',
                IntegerType::class,
                [
                    'label' => 'Priorytet wyświetlania',
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'label' => 'Cena',
                    'currency' => 'PLN',
                ]
            )
            ->add(
                'active',
                ChoiceType::class,
                [
                    'label' => 'Pokaż na stronie',
                    'choices' => array(
                        'Tak' => true,
                        'Nie' => false,
                    ),
                ]
            )
            ->add(
                'metaDescription',
                TextareaType::class,
                [
                    'label' => 'Metatag - Opis',
                    'attr' => [
                        'rows' => 5,
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'metaTitle',
                TextareaType::class,
                [
                    'label' => 'Metatag - Tytuł',
                    'attr' => [
                        'rows' => 5,
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'content',
                TextareaType::class,
                [
                    'label' => 'Treść',
                    'required' => false,
                    'attr' => [
                        'rows' => 25,
                    ],
                ]
            )
            ->add(
                'testimonialContent',
                TextareaType::class,
                [
                    'label' => 'Kod poniżej treści',
                    'required' => false,
                    'attr' => [
                        'rows' => 15,
                    ],
                ]
            )
            ->add(
                'shortContent',
                TextareaType::class,
                [
                    'label' => 'Krótka treść',
                    'required' => false,
                    'attr' => [
                        'rows' => 3,
                    ],
                ]
            )
            ->add(
                'images',
                CollectionType::class,
                array(
                    'entry_type' => ImageType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'label' => 'Zdjęcia',
                    'prototype' => true,
                    'prototype_name' => '__parent_name__',
                    'entry_options' => [
                        'label' => 'Zdjęcie',
                    ],
                    'attr' => array(
                        'class' => 'form-collection',
                    ),
                )
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'label' => 'Kategoria',
                    'class' => 'AppBundle:Category',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\Product',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }

}
