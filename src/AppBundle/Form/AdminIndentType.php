<?php



namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Shipping;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdminIndentType
 *
 * @package AppBundle\Form
 */
class AdminIndentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add(
                        'invoice',
                        ChoiceType::class,
                        [
                                'label' => false,
                                'choices' => [
                                        'Nie' => false,
                                        'Tak' => true,
                                ],
                        ]
                )
                ->add(
                        'disableShipping',
                        HiddenType::class
                )
                ->add(
                        'nip',
                        TextType::class,
                        [
                                'label' => 'NIP',
                                'required' => false,
                        ]
                )
                ->add(
                        'phoneNumber',
                        TextType::class,
                        [
                                'label' => 'Numer telefonu',
                        ]
                )
                ->add(
                        'email',
                        EmailType::class,
                        [
                                'label' => 'Adres Email',
                        ]
                )
                ->add(
                        'shippingAddress',
                        AddressType::class,
                        [
                                'label' => false,
                        ]
                )
                ->add(
                        'invoiceAddress',
                        AddressType::class,
                        [
                                'label' => false,
                                'required' => false,
                        ]
                )
                ->add(
                        'shipping',
                        EntityType::class,
                        [
                                'required' => false,
                                'label' => false,
                                'class' => Shipping::class,
                        ]
                )
                ->add(
                        'country',
                        EntityType::class,
                        [
                                'required' => false,
                                'label' => 'Kraj',
                                'class' => Country::class,

                        ]
                )
                ->add(
                        'info',
                        TextareaType::class,
                        [
                                'label' => 'Dodatkowe info',
                                'attr' => [
                                        'placeholder' => 'Maks 1020 znaków',
                                ],
                                'required' => false,
                        ]
                )
                ->add(
                        'adminNotes',
                        TextareaType::class,
                        [
                                'label' => 'Notatki admina',
                                'attr' => [
                                        'rows' => 10,
                                ],
                                'required' => false,
                        ]
                )
                ->add(
                        'payed',
                        CheckboxType::class,
                        [
                                'label' => 'Zapłacono',
                                'required' => false,
                        ]
                )
                ->add(
                        'ended',
                        CheckboxType::class,
                        [
                                'label' => 'Zakończono',
                                'required' => false,
                        ]
                )
                ->add(
                        'paczkomat',
                        TextType::class,
                        [
                                'label' => 'Wybrany paczkomat',
                                'required' => false,
                        ]
                )
                ->add(
                        'user',
                        EntityType::class,
                        [
                                'class' => User::class,
                                'label' => 'Użytkownik',
                                'required' => false,
                                'placeholder' => '---brak---',
                        ]
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
                array(
                        'data_class' => 'AppBundle\Entity\Indent',
                        'validation_groups' => function (FormInterface $form) {
                            $data = $form->getData();

                            $groups = ['Default'];

                            if ($data->getInvoice()) {
                                $groups [] = 'invoice';
                            }



                            if ($data->getDisableShipping()) {
                                $groups [] = 'subscription';
                            }else{
                                $groups [] = 'phone';

                            }

                            if ($data->getShipping() && $data->getShipping()->getType() == 'inpost-paczkomaty') {
                                $groups[] = 'paczkomat';
                            }

                            return $groups;
                        },
                )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_indent';
    }

}
