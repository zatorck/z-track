<?php


namespace AppBundle\Form;

use AppBundle\AppSettings;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeviceType
 *
 * @package AppBundle\Form
 */
class DeviceType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add(
                        'serialNumber',
                        TextType::class,
                        [
                                'label' => 'Numer seryjny',
                        ]
                )
                ->add(
                        'name',
                        TextType::class,
                        [
                                'label' => 'Nazwa',
                        ]
                )
                ->add(
                        'endAt',
                        DateType::class,
                        [
                                'label' => 'Ważny do',
                                'widget' => 'single_text',
                                'format' => 'dd-MM-yyyy',
                                'attr' => [
                                        'class' => 'datepicker',
                                        'autocomplete' => 'off',
                                ],
                        ]
                )
                ->add(
                        'simNumber',
                        TextType::class,
                        [
                                'label' => 'Sim - numer',
                                'required' => false,
                        ]
                )
                ->add(
                        'simEndAt',
                        DateType::class,
                        [
                                'label' => 'Sim ważny do',
                                'widget' => 'single_text',
                                'required' => false,
                                'format' => 'dd-MM-yyyy',
                                'attr' => [
                                        'class' => 'datepicker',
                                        'autocomplete' => 'off',
                                ],
                        ]
                )
                ->add(
                        'configured',
                        ChoiceType::class,
                        [
                                'label' => 'Czy urządzenie jest poprawnie skonfigurowane?',
                                'choices' => [
                                        'Nie' => false,
                                        'Tak' => true,
                                ],
                        ]
                )
                ->add(
                        'users',
                        EntityType::class,
                        [
                                'multiple' => true,
                                'label' => 'Użytkownicy',
                                'class' => User::class,
                        ]
                )
                ->add(
                        'notice',
                        TextareaType::class,
                        [
                                'label' => 'Notatka ogólna',
                                'required' => false,
                                'attr' => [
                                        'rows' => 5,
                                ],
                        ]
                );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
                array(
                        'data_class' => 'AppBundle\Entity\Device',
                )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_device';
    }

}
