<?php



namespace AppBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

/**
 * Class RegistrationType
 *
 * @package AppBundle\Form
 */
class RegistrationType extends AbstractType
{

    /**
     * @param  FormBuilderInterface  $builder
     * @param  array  $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->remove('username')
                ->add('recaptcha', EWZRecaptchaType::class, [
                        'label' => false,
                        'mapped'      => false,
                        'constraints' => array(
                                new RecaptchaTrue()
                        )
                ]);
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}
