<?php


namespace AppBundle\Request;

/**
 * Interface RequestInterface
 *
 * @package AppBundle\Request
 */
interface RequestInterface
{
    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function getUri(): string;

    /**
     * @return array|null
     */
    public function send(): ?array;
}
