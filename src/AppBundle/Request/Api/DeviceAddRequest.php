<?php

namespace AppBundle\Request\Api;

use AppBundle\Entity\Device;
use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeviceAddRequest
 *
 * @package AppBundle\Request\Api
 */
class DeviceAddRequest implements RequestInterface
{
    /**
     * @var Device|null
     */
    private $device;

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var PermissionAddRequest
     */
    private $permissionAddRequest;


    /**
     * DeviceAddRequest constructor.
     *
     * @param  ApiUtils  $apiUtils
     * @param  LogFactory  $logFactory
     * @param  SessionInterface  $session
     * @param  PermissionAddRequest  $permissionAddRequest
     */
    public function __construct(
            ApiUtils $apiUtils,
            LogFactory $logFactory,
            SessionInterface $session,
            PermissionAddRequest $permissionAddRequest
    ) {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
        $this->session = $session;
        $this->permissionAddRequest = $permissionAddRequest;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param  mixed  $device
     *
     * @return DeviceAddRequest
     */
    public function setDevice(Device $device): self
    {
        $this->device = $device;

        return $this;
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'post';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'devices';
    }


    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {
        $now = new \DateTime();

        $client = $this->apiUtils->getClient();

        try {
            $response = $client->request($this->getMethod(), $this->getUri(), [
                    'json' => [
                            'name' => $this->device->getName(),
                            'uniqueId' => $this->device->getSerialNumber(),
                            'disabled' => ($now > $this->device->getEndAt()) ? true : false,

                    ],

                'verify' => false
            ]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());
            $this->session->getBag('flashes')->add('error', 'Wystąpił błąd w API, sprawdź logi, powiadom admina!');

            return null;
        }
        $body = json_decode($response->getBody(), true);

        $this->device->setDisabledInTraccar($body['disabled']);

        foreach($this->device->getUsers() as $user){
            $this->permissionAddRequest
                    ->setDeviceTraccarId($body['id'])
                    ->setUserTraccarId(
                            $user->getTraccarId()
                    )->send();
        }


        return $body;
    }

}
