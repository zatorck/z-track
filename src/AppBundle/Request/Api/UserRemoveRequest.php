<?php


namespace AppBundle\Request\Api;


use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class UserRemoveRequest
 *
 * @package AppBundle\Request\Api
 */
class UserRemoveRequest implements RequestInterface
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * UserAddRequest constructor.
     *
     * @param  ApiUtils  $apiUtils
     * @param  LogFactory  $logFactory
     */
    public function __construct(
            ApiUtils $apiUtils,
            LogFactory $logFactory,
            SessionInterface $session
    ) {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
        $this->session = $session;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param  mixed  $user
     *
     * @return UserRemoveRequest
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'delete';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'users/'.$this->user->getTraccarId();
    }


    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {
        $client = $this->apiUtils->getClient();

        try {
            $response = $client->request($this->getMethod(), $this->getUri(), ['verify' => false]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());

            $this->session->getBag('flashes')->add('error', 'Wystąpił błąd w API, sprawdź logi, powiadom admina!');

            return null;
        }

        return json_decode($response->getBody(), true);
    }

}
