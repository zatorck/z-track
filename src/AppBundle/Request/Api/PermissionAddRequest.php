<?php

namespace AppBundle\Request\Api;

use AppBundle\Entity\Device;
use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class PermissionAddRequest
 *
 * @package AppBundle\Request\Api
 */
class PermissionAddRequest implements RequestInterface
{
    /**
     * @var string|int|null
     */
    private $deviceTraccarId;
    /**
     * @var string|int|null
     */
    private $userTraccarId;

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * PermissionAddRequest constructor.
     *
     * @param ApiUtils $apiUtils
     * @param LogFactory $logFactory
     */
    public function __construct(
        ApiUtils         $apiUtils,
        LogFactory       $logFactory,
        SessionInterface $session
    )
    {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
        $this->session = $session;
    }

    /**
     * @return int|string|null
     */
    public function getDeviceTraccarId()
    {
        return $this->deviceTraccarId;
    }

    /**
     * @param int|string|null $deviceTraccarId
     *
     * @return PermissionAddRequest
     */
    public function setDeviceTraccarId($deviceTraccarId)
    {
        $this->deviceTraccarId = $deviceTraccarId;

        return $this;
    }

    /**
     * @return int|string|null
     */
    public function getUserTraccarId()
    {
        return $this->userTraccarId;
    }

    /**
     * @param int|string|null $userTraccarId
     *
     * @return PermissionAddRequest
     */
    public function setUserTraccarId($userTraccarId)
    {
        $this->userTraccarId = $userTraccarId;

        return $this;
    }

    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {
        $client = $this->apiUtils->getClient();

        try {
            $response = $client->request($this->getMethod(), $this->getUri(), [
                'json' => [
                    'userId' => $this->userTraccarId,
                    'deviceId' => $this->deviceTraccarId,
                ],
                'verify' => false
            ]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());
            $this->session->getBag('flashes')->add('error', 'Wystąpił błąd w API, sprawdź logi, powiadom admina!');

            return null;
        }

        return json_decode($response->getBody(), true);
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'post';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'permissions';
    }

}
