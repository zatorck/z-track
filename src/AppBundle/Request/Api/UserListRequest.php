<?php


namespace AppBundle\Request\Api;


use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class UserListRequest
 *
 * @package AppBundle\Request\Api
 */
class UserListRequest implements RequestInterface
{

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * UserAddRequest constructor.
     *
     * @param  ApiUtils  $apiUtils
     * @param  LogFactory  $logFactory
     */
    public function __construct(
            ApiUtils $apiUtils,
            LogFactory $logFactory
    ) {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'get';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'users';
    }


    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {
        $client = $this->apiUtils->getClient();

        try {
            $response = $client->request($this->getMethod(), $this->getUri(), ['verify' => false]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());

            return null;
        }

        return json_decode($response->getBody(), true);
    }

}
