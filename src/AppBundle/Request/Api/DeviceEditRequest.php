<?php

namespace AppBundle\Request\Api;

use AppBundle\Entity\Device;
use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class DeviceEditRequest
 *
 * @package AppBundle\Request\Api
 */
class DeviceEditRequest implements RequestInterface
{
    /**
     * @var Device|null
     */
    private $device;

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var PermissionAddRequest
     */
    private $permissionAddRequest;
    /**
     * @var PermissionRemoveRequest
     */
    private $permissionRemoveRequest;
    /**
     * @var array
     */
    private $changeSet = [];

    /**
     * DeviceEditRequest constructor.
     *
     * @param  ApiUtils  $apiUtils
     * @param  LogFactory  $logFactory
     * @param  SessionInterface  $session
     * @param  PermissionAddRequest  $permissionAddRequest
     */
    public function __construct(
            ApiUtils $apiUtils,
            LogFactory $logFactory,
            SessionInterface $session,
            PermissionAddRequest $permissionAddRequest,
            PermissionRemoveRequest $permissionRemoveRequest
    ) {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
        $this->session = $session;
        $this->permissionAddRequest = $permissionAddRequest;
        $this->permissionRemoveRequest = $permissionRemoveRequest;
    }

    /**
     * @return mixed
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param  mixed  $device
     *
     * @return DeviceEditRequest
     */
    public function setDevice(Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @param  array  $changeSet
     *
     * @return DeviceEditRequest
     */
    public function setChangeSet(array $changeSet): DeviceEditRequest
    {
        $this->changeSet = $changeSet;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'put';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'devices/'.$this->device->getTraccarId();
    }


    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {

        $now = new \DateTime();
        $client = $this->apiUtils->getClient();
        try {
            $response = $client->request($this->getMethod(), $this->getUri(), [
                    'json' => [
                            'id' => $this->device->getTraccarId(),
                            'name' => $this->device->getName(),
                            'uniqueId' => $this->device->getSerialNumber(),
                            'disabled' => ($now > $this->device->getEndAt()) ? true : false,
                    ],
                'verify' => false
            ]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());
            $this->session->getBag('flashes')->add('error', 'Wystąpił błąd w API, sprawdź logi, powiadom admina!');

            return null;
        }
        $body = json_decode($response->getBody(), true);

        $this->device->setDisabledInTraccar($body['disabled']);

        foreach ($this->device->getUserChangeSet() as $user) {
            $this->permissionRemoveRequest
                    ->setDeviceTraccarId($body['id'])
                    ->setUserTraccarId(
                            $user->getTraccarId()
                    )->send();

        }

        foreach ($this->device->getUsers() as $user) {
            $this->permissionAddRequest
                    ->setDeviceTraccarId($body['id'])
                    ->setUserTraccarId(
                            $user->getTraccarId()
                    )->send();

        }


        return $body;
    }

}
