<?php

namespace AppBundle\Request\Api;

use AppBundle\Entity\User;
use AppBundle\Factory\LogFactory;
use AppBundle\Request\RequestInterface;
use AppBundle\Utils\ApiUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class UserEditRequest
 *
 * @package AppBundle\Request\Api
 */
class UserEditRequest implements RequestInterface
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * @var ApiUtils
     */
    private $apiUtils;

    /**
     * @var LogFactory
     */
    private $logFactory;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * UserEditRequest constructor.
     *
     * @param  ApiUtils  $apiUtils
     * @param  LogFactory  $logFactory
     */
    public function __construct(
            ApiUtils $apiUtils,
            LogFactory $logFactory,
            SessionInterface $session
    ) {
        $this->apiUtils = $apiUtils;
        $this->logFactory = $logFactory;
        $this->session = $session;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param  mixed  $user
     *
     * @return UserEditRequest
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }


    /**
     * @return string
     */
    public function getMethod(): string
    {
        return 'put';
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return 'users/'.$this->user->getTraccarId();
    }


    /**
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?array
    {
        $client = $this->apiUtils->getClient();
        $data = [
                'id' => $this->user->getTraccarId(),
                'name' => $this->user->getEmail(),
                'email' => $this->user->getEmail(),
                'readonly' => $this->user->getTraccarReadonly(),
                'administrator' => $this->user->getTraccarAdministrator(),
                'deviceReadonly' => $this->user->getTraccarDeviceReadonly(),
                'token' => $this->user->getTraccarToken(),
        ];

        if ($this->user->getPlainPassword()) {
            $data['password'] = $this->user->getPlainPassword();
        } elseif ($this->user->getChangingPassword()) {
            $data['password'] = $this->user->getChangingPassword();
        }


        try {
            $response = $client->request($this->getMethod(), $this->getUri(), [
                    'json' => $data,
                'verify' => false
            ]);
        } catch (\Exception $e) {
            $this->logFactory->create('traccar_api_error', $e->getMessage());
            $this->session->getBag('flashes')->add('error', 'Wystąpił błąd w API, sprawdź logi, powiadom admina!');

            return null;
        }


        return json_decode($response->getBody(), true);
    }

}
