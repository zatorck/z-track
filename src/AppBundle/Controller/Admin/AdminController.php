<?php


namespace AppBundle\Controller\Admin;

use AppBundle\AppSettings;
use AppBundle\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="admin", methods={"GET"})
     */
    public function admin(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $productCount = $em->getRepository('AppBundle:Product')->count([]);
        $categoryCount = $em->getRepository('AppBundle:Category')->count([]);
        $devicesCount = $em->getRepository('AppBundle:Device')->count([]);
        $userCount = $em->getRepository('AppBundle:User')->count([]);
        $activeDevices = $em->getRepository('AppBundle:Device')->count(['disabledInTraccar' => false]);
        $subscriptionCount = $em->getRepository('AppBundle:Subscription')->count([]);
        $devicesToEndQuery = $em->getRepository('AppBundle:Device')->getEndingQuery();

        $query = $em->getRepository('AppBundle:Indent')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );
        $paginationDeviceToEnd = $paginator->paginate(
                $devicesToEndQuery,
                $request->query->getInt('otherPage', 1),
                50,
                [
                        'pageParameterName' => 'otherPage',
                        'sortFieldParameterName' => 'otherSort'
                ]
        );

        return $this->render(
                'admin/dashboard.html.twig',
                [
                        'productCount' => $productCount,
                        'categoryCount' => $categoryCount,
                        'deviceCount' => $devicesCount,
                        'userCount' => $userCount,
                        'activeDevices' => $activeDevices,
                        'subscriptionCount' => $subscriptionCount,
                        'pagination' => $pagination,
                        'paginationDeviceToEnd' => $paginationDeviceToEnd,
                ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/send-email", name="admin_send_email", methods={"GET", "POST"})
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createFormBuilder([])
                ->add('email', EmailType::class, [
                        'label' => 'Adresat',
                ])
                ->add('topic', TextType::class, [
                        'label' => 'Temat',
                ])
                ->add('content', TextareaType::class, [
                        'label' => 'Treść',
                        'attr' => [
                                'rows' => 10,
                        ],
                ])
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();

            //--------------- sending mails
            $message = (new \Swift_Message($data['topic']))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo($data['email'])
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->renderView(
                                    'emails/admin/email.html.twig',
                                    ['data' => $data]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails
            $this->addFlash('success', 'Email został wysłany');

            return $this->redirectToRoute('admin_send_email');
        }

        return $this->render(
                'admin/sendEmail.html.twig',
                [
                        'form' => $form->createView(),
                ]
        );
    }

}
