<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Partner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PartnerController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/partner")
 */
class PartnerController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_partner_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Partner')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/partner/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_partner_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $partner = new Partner();
        $form = $this->createForm('AppBundle\Form\PartnerType', $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                'admin_partner_edit',
                array('id' => $partner->getId())
            );
        }

        return $this->render(
            'admin/partner/new.html.twig',
            array(
                'partner' => $partner,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request  $request
     * @param Partner $partner
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_partner_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Partner $partner): Response
    {

        $form = $this->createForm('AppBundle\Form\PartnerType', $partner);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_partner_edit',
                array('id' => $partner->getId())
            );
        }

        return $this->render(
            'admin/partner/new.html.twig',
            array(
                'partner' => $partner,
                'form' => $form->createView(),
                'edit' => true,
            )
        );
    }

    /**
     * @param Partner $partner
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_partner_delete", methods={"GET"})
     */
    public function delete(Partner $partner): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($partner);
        $em->flush();

        return $this->redirectToRoute('admin_partner_index');
    }

}
