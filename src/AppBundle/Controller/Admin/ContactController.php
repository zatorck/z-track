<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/contact")
 */
class ContactController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/", name="admin_contact_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Contact')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );

        return $this->render(
                'admin/contact/index.html.twig',
                array(
                        'pagination' => $pagination,
                )
        );
    }


    /**
     * @param  Contact  $contact
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_contact_delete", methods={"GET"})
     */
    public function delete(Contact $contact): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($contact);
        $em->flush();

        return $this->redirectToRoute('admin_contact_index');
    }

}
