<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Country;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CountryController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/country")
 */
class CountryController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_country_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Country')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/country/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_country_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $country = new Country();
        $form = $this->createForm('AppBundle\Form\CountryType', $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                'admin_country_edit',
                array('id' => $country->getId())
            );
        }

        return $this->render(
            'admin/country/new.html.twig',
            array(
                'country' => $country,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request $request
     * @param Country $country
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_country_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Country $country): Response
    {

        $form = $this->createForm('AppBundle\Form\CountryType', $country);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_country_edit',
                array('id' => $country->getId())
            );
        }

        return $this->render(
            'admin/country/new.html.twig',
            array(
                'country' => $country,
                'form' => $form->createView(),
                'edit' => true,
            )
        );
    }

    /**
     * @param Country $country
     *
     * @return Response
     *
     * @Route("/{id}", name="admin_country_delete", methods={"GET"})
     */
    public function delete(Country $country): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($country);
        $em->flush();

        return $this->redirectToRoute('admin_country_index');
    }

}
