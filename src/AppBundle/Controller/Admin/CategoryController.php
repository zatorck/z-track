<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/category")
 */
class CategoryController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_category_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Category')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/category/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_category_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm('AppBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                'admin_category_edit',
                array('id' => $category->getId())
            );
        }

        return $this->render(
            'admin/category/new.html.twig',
            array(
                'category' => $category,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request  $request
     * @param Category $category
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_category_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Category $category): Response
    {

        $form = $this->createForm('AppBundle\Form\CategoryType', $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_category_edit',
                array('id' => $category->getId())
            );
        }

        return $this->render(
            'admin/category/new.html.twig',
            array(
                'category' => $category,
                'form' => $form->createView(),
                'edit' => true,
            )
        );
    }

    /**
     * @param Category $category
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_category_delete", methods={"GET"})
     */
    public function delete(Category $category): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('admin_category_index');
    }

}
