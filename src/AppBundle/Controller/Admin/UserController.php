<?php



namespace AppBundle\Controller\Admin;

use AppBundle\AppSettings;
use AppBundle\Entity\User;
use AppBundle\Form\RegistrationType;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/user")
 */
class UserController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/", name="admin_user_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:User')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );

        return $this->render(
                'admin/user/index.html.twig',
                array(
                        'pagination' => $pagination,
                )
        );
    }

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_user_new", methods={"GET", "POST"})
     */
    public function new(Request $request, \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setConfirmationToken(null);
            $user->setEnabled(true);
            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Twoje konto na portalu z-track.com zostało założone'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo($user->getEmail())
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->renderView(
                                    'emails/user/newUser.html.twig',
                                    ['user' => $user]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails

            $em->persist($user);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                    'admin_user_edit',
                    array('id' => $user->getId())
            );
        }

        return $this->render(
                'admin/user/new.html.twig',
                array(
                        'user' => $user,
                        'form' => $form->createView(),
                )
        );
    }

    /**
     * @param  Request  $request
     * @param  User  $user
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}/edit", name="admin_user_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, User $user, \Swift_Mailer $mailer): Response
    {

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);

            if ($form->get("sendEmail")->getData()) //--------------- sending mails
            {
                $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Twoje konto na portalu z-track.com zmieniło hasło'))
                        ->setFrom('noreply@z-track.com', 'z-Track')
                        ->setTo($user->getEmail())
                        ->setReplyTo(AppSettings::ADMIN_EMAIL)
                        ->setBody(
                                $this->renderView(
                                        'emails/user/editUser.html.twig',
                                        ['user' => $user]
                                ),
                                'text/html'
                        );
                $mailer->send($message);

            }

            //--------------- sending mails
            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                    'admin_user_edit',
                    array('id' => $user->getId())
            );
        }

        return $this->render(
                'admin/user/new.html.twig',
                array(
                        'user' => $user,
                        'form' => $form->createView(),
                        'edit' => true,
                )
        );
    }

    /**
     * @param  User  $user
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_user_delete", methods={"GET"})
     */
    public function delete(User $user): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_user_index');
    }

}
