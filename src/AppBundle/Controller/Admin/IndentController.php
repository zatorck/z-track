<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Indent;
use AppBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndentController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/indent")
 */
class IndentController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_indent_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Indent')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/indent/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     * @param Indent  $indent
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_indent_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Indent $indent): Response
    {
        $form = $this->createForm('AppBundle\Form\AdminIndentType', $indent);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneByEmail($indent->getEmail());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_indent_edit',
                array('id' => $indent->getId())
            );
        }

        return $this->render(
            'admin/indent/new.html.twig',
            array(
                'indent' => $indent,
                'form' => $form->createView(),
                'edit' => true,
                'user' => $user,
            )
        );
    }

    /**
     * @param Indent $indent
     *
     * @return Response
     *
     * @Route("/{id}", name="admin_indent_delete", methods={"GET"})
     */
    public function delete(Indent $indent): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($indent);
        $em->flush();

        return $this->redirectToRoute('admin_indent_index');
    }

}
