<?php



namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LogController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/log")
 */
class LogController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/", name="admin_log_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Log')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );

        return $this->render(
                'admin/log/index.html.twig',
                array(
                        'pagination' => $pagination,
                )
        );
    }


}
