<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/product")
 */
class ProductController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_product_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Product')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/product/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_product_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm('AppBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                'admin_product_edit',
                array('id' => $product->getId())
            );
        }

        return $this->render(
            'admin/product/new.html.twig',
            array(
                'product' => $product,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_product_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Product $product): Response
    {

        $form = $this->createForm('AppBundle\Form\ProductType', $product);

        $originalImages = new \Doctrine\Common\Collections\ArrayCollection;

        foreach ($product->getImages() as $image) {
            $originalImages->add($image);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($originalImages as $image) {
                if (false === $product->getImages()->contains($image)) {
                    $em->remove($image);
                }
            }

            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_product_edit',
                array('id' => $product->getId())
            );
        }

        return $this->render(
            'admin/product/new.html.twig',
            array(
                'product' => $product,
                'form' => $form->createView(),
                'edit' => true,
            )
        );
    }

    /**
     * @param Product $product
     *
     * @return Response
     *
     * @Route("/{id}", name="admin_product_delete", methods={"GET"})
     */
    public function delete(Product $product): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('admin_product_index');
    }

}
