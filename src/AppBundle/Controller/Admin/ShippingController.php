<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Shipping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShippingController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/shipping")
 */
class ShippingController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="admin_shipping_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Shipping')->getAllQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            50
        );

        return $this->render(
            'admin/shipping/index.html.twig',
            array(
                'pagination' => $pagination,
            )
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/new", name="admin_shipping_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $shipping = new Shipping();
        $form = $this->createForm('AppBundle\Form\ShippingType', $shipping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($shipping);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                'admin_shipping_edit',
                array('id' => $shipping->getId())
            );
        }

        return $this->render(
            'admin/shipping/new.html.twig',
            array(
                'shipping' => $shipping,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Request  $request
     * @param Shipping $shipping
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="admin_shipping_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Shipping $shipping): Response
    {
        $form = $this->createForm('AppBundle\Form\ShippingType', $shipping);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                'admin_shipping_edit',
                array('id' => $shipping->getId())
            );
        }

        return $this->render(
            'admin/shipping/new.html.twig',
            array(
                'shipping' => $shipping,
                'form' => $form->createView(),
                'edit' => true,
            )
        );
    }

    /**
     * @param Shipping $shipping
     *
     * @return Response
     *
     * @Route("/{id}", name="admin_shipping_delete", methods={"GET"})
     */
    public function delete(Shipping $shipping): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($shipping);
        $em->flush();

        return $this->redirectToRoute('admin_shipping_index');
    }

}
