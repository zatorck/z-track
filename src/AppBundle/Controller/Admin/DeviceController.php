<?php


namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Device;
use http\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DeviceController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/device")
 */
class DeviceController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/", name="admin_device_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Device')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );

        return $this->render(
                'admin/device/index.html.twig',
                array(
                        'pagination' => $pagination,
                )
        );
    }

    /**
     * @param  Request  $request
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/new", name="admin_device_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $device = new Device();
        $device->setDisabledInTraccar(false);
        $form = $this->createForm('AppBundle\Form\DeviceType', $device);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($device);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                    'admin_device_edit',
                    array('id' => $device->getId())
            );
        }

        return $this->render(
                'admin/device/new.html.twig',
                array(
                        'device' => $device,
                        'form' => $form->createView(),
                )
        );
    }

    /**
     * @param  Request  $request
     * @param  Device  $device
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}/edit", name="admin_device_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Device $device): Response
    {

        $form = $this->createForm('AppBundle\Form\DeviceType', $device);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                    'admin_device_edit',
                    array('id' => $device->getId())
            );
        }

        return $this->render(
                'admin/device/new.html.twig',
                array(
                        'device' => $device,
                        'form' => $form->createView(),
                        'edit' => true,
                )
        );
    }

    /**
     * @param  Request  $request
     * @param  Device  $device
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}/update-name", name="admin_device_update_name", methods={"POST"})
     */
    public function updateName(Request $request, Device $device): Response
    {

        $device->setName($request->request->get('name'));

        $em = $this->getDoctrine()->getManager();

        $em->persist($device);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @param  Device  $device
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_device_delete", methods={"GET"})
     */
    public function delete(Device $device): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($device);
        $em->flush();

        return $this->redirectToRoute('admin_device_index');
    }

}
