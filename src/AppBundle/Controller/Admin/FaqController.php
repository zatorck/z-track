<?php



namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Faq;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FaqController
 *
 * @package AppBundle\Controller\Admin
 *
 * @Route("/core/admin/faq")
 */
class FaqController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     *
     * @Route("/", name="admin_faq_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Faq')->getAllQuery($request->query->get('query'));
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
        );

        return $this->render(
                'admin/faq/index.html.twig',
                array(
                        'pagination' => $pagination,
                )
        );
    }

    /**
     * @param  Request  $request
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/new", name="admin_faq_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $faq = new Faq();
        $form = $this->createForm('AppBundle\Form\FaqType', $faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($faq);
            $em->flush();
            $this->addFlash('info', 'Obiekt został dodany poprawnie.');

            return $this->redirectToRoute(
                    'admin_faq_edit',
                    array('id' => $faq->getId())
            );
        }

        return $this->render(
                'admin/faq/new.html.twig',
                array(
                        'faq' => $faq,
                        'form' => $form->createView(),
                )
        );
    }

    /**
     * @param  Request  $request
     * @param  Faq  $faq
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}/edit", name="admin_faq_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Faq $faq): Response
    {

        $form = $this->createForm('AppBundle\Form\FaqType', $faq);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $this->addFlash('info', 'Obiekt został edytowany poprawnie.');

            return $this->redirectToRoute(
                    'admin_faq_edit',
                    array('id' => $faq->getId())
            );
        }

        return $this->render(
                'admin/faq/new.html.twig',
                array(
                        'faq' => $faq,
                        'form' => $form->createView(),
                        'edit' => true,
                )
        );
    }

    /**
     * @param  Faq  $faq
     *
     * @return Response
     *
     * @Route("/{id}/delete", name="admin_faq_delete", methods={"GET"})
     */
    public function delete(Faq $faq): Response
    {
        $this->addFlash('warning', 'Obiekt został usnięty poprawnie.');

        $em = $this->getDoctrine()->getManager();
        $em->remove($faq);
        $em->flush();

        return $this->redirectToRoute('admin_faq_index');
    }

}
