<?php


namespace AppBundle\Controller;

use Allset\Przelewy24Bundle\Factory\ProcessFactory;
use AppBundle\AppSettings;
use AppBundle\Entity\Indent;
use AppBundle\Event\IndentCreateEvent;
use AppBundle\Events;
use AppBundle\Factory\IndentItemFactory;
use AppBundle\Form\IndentSubscriptionType;
use AppBundle\Form\IndentSubscriptionTypeConfirm;
use AppBundle\Form\IndentType;
use AppBundle\Helper\Bridge\Przelewy24\UrlHelper;
use AppBundle\Helper\Session\CartCleanerHelper;
use AppBundle\Helper\Session\CartHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndentController
 *
 * @package AppBundle\Controller
 */
class IndentController extends Controller
{

    /**
     * @param  CartCleanerHelper  $cartCleaner
     * @param  Request  $request
     * @param  UrlHelper  $urlHelper
     * @param  IndentItemFactory  $indentItemFactory
     * @param  EventDispatcherInterface  $eventDispatcher
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/zamowienie/stworz", name="indent_create", methods={"GET", "POST"})
     */
    public function create(
            CartCleanerHelper $cartCleaner,
            Request $request,
            UrlHelper $urlHelper,
            IndentItemFactory $indentItemFactory,
            EventDispatcherInterface $eventDispatcher,
            \Swift_Mailer $mailer
    ): Response {
        $cartCleaner->compareCartItems();
        $indent = new Indent();

        $eventDispatcher->dispatch(Events::INDENT_CREATE, new IndentCreateEvent($indent));

        $form = $this->createForm(IndentType::class, $indent, []);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $indent->setTempEnded(true);

            $em = $this->getDoctrine()->getManager(); // entity manager

            // Ostateczne sprawdzenie czy karta nie jest pusta
            if (!count($indentItemFactory->createGroupFromSession($indent))) {
                $this->addFlash('error', 'Twoja karta jest pusta');

                return $this->redirectToRoute('indent_create');
            }


            $indent->invoiceDefaultToUser();

            // Zapisywanie indent i indent items do bazy danych
            $em->persist($indent);
            $em->flush();


            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Do admina - Nowe zamówienie'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo(AppSettings::ADMIN_EMAIL)
                    ->setReplyTo($indent->getEmail())
                    ->setBody(
                            $this->renderView(
                                    'emails/admin/indent.html.twig',
                                    ['indent' => $indent]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails

            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Dziękujemy za zamówienie'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo($indent->getEmail())
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->renderView(
                                    'emails/user/indent.html.twig',
                                    ['indent' => $indent]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails

            if (!$indent->getShipping()->getCashOnDelivery()) {
                return $this->redirect($urlHelper->getUrl($indent));
            }


            return $this->redirectToRoute('indent_return');
        }

        return $this->render(
                'indent.html.twig',
                [
                        'form' => $form->createView(),
                ]
        );
    }


    /**
     * @Route("/user/zamowienie/przedluz-abonament", name="indent_create_with_subscription", methods={"GET","POST"})
     */
    public function createWithSubscription(
            Request $request,
            UrlHelper $urlHelper,
            IndentItemFactory $indentItemFactory,
            EventDispatcherInterface $eventDispatcher,
            \Swift_Mailer $mailer
    ): Response {

        $indent = new Indent();

        $indent->setDisableShipping(true);

        $eventDispatcher->dispatch(Events::INDENT_CREATE, new IndentCreateEvent($indent));

        $form = $this->createForm(IndentSubscriptionType::class, $indent, []);
        $form->handleRequest($request);

        if ($form->isValid() && count($indent->getIndentSubscriptions())) {
            $em = $this->getDoctrine()->getManager();

            $indent->setEmail($this->getUser()->getEmail());


            $subscription = $form->get('subscription')->getData();

            foreach ($indent->getIndentSubscriptions() as $indentSubscription) {
                $indentSubscription->setPrice($subscription->getPrice());
                $indentSubscription->setMonths($subscription->getMonths());
                $indentSubscription->setSubscription($subscription);
                $indentSubscription->setIndent($indent);
            }


            $indent->setTempEnded(false);

            // Zapisywanie indent i indent items do bazy danych
            $em->persist($indent);


            $em->flush();

            return $this->redirectToRoute('indent_create_with_subscription_confirm', ['token' => $indent->getToken()]);
        } elseif ($form->isValid()) {
            $this->addFlash('error', 'Wybierz conajmniej jedno urządzenie do przedłużenia');
        }

        return $this->render(
                'indentSubscription.html.twig',
                [
                        'form' => $form->createView(),
                ]
        );

    }


    /**
     * @Route("/user/zamowienie/przedluz-abonament/{token}", name="indent_create_with_subscription_confirm", methods={"GET","POST"})
     */
    public function createWithSubscriptionConfirm(
            Indent $indent,
            Request $request,
            UrlHelper $urlHelper,
            IndentItemFactory $indentItemFactory,
            EventDispatcherInterface $eventDispatcher,
            \Swift_Mailer $mailer
    ): Response {

        $indent->setDisableShipping(true);


        $form = $this->createForm(IndentSubscriptionTypeConfirm::class, $indent, []);
        $form->handleRequest($request);

        if ($form->isValid() && count($indent->getIndentSubscriptions())) {
            $indent->setTempEnded(true);

            $em = $this->getDoctrine()->getManager();


            // Zapisywanie indent i indent items do bazy danych
            $em->persist($indent);

            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Do admina - Nowe zamówienie - abonament'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo(AppSettings::ADMIN_EMAIL)
                    ->setReplyTo($indent->getEmail())
                    ->setBody(
                            $this->renderView(
                                    'emails/admin/indent.html.twig',
                                    ['indent' => $indent]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails

            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Dziękujemy za zamówienie abonamentu'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo($indent->getEmail())
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->renderView(
                                    'emails/user/indent.html.twig',
                                    ['indent' => $indent]
                            ),
                            'text/html'
                    );

            $mailer->send($message);

            //--------------- sending mails

            $indent->invoiceDefaultToUser();
            $em->flush();


            return $this->redirect($urlHelper->getUrl($indent));
        } elseif ($form->isValid()) {
            $this->addFlash('error', 'Wybierz conajmniej jedno urządzenie do przedłużenia');

            return $this->redirectToRoute('indent_create_with_subscription');
        }

        return $this->render(
                'indentSubscriptionConfirm.html.twig',
                [
                        'form' => $form->createView(),
                        'indent' => $indent,
                ]
        );

    }

    /**
     * @return Response
     *
     * @Route("/zamowienie/dziekujemy", name="indent_return", methods={"GET"})
     */
    public function return(): Response
    {
        return $this->render('indent/thanks.html.twig');
    }

    /**
     * @return Response
     *
     * @Route("/user/zamowienie", name="user_indent_user", methods={"GET"})
     */
    public function user(): Response
    {
        return $this->redirectToRoute('indent_create');
    }

}
