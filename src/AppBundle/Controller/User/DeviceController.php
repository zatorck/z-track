<?php



namespace AppBundle\Controller\User;

use AppBundle\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IntentController
 *
 * @package AppBundle\Controller\User
 *
 * @Route("/uzytkownik")
 */
class DeviceController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/urzadzenia", name="user_device_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Device::class)->getByUserIdQuery($this->getUser()->getId());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            50/*limit per page*/
        );

        return $this->render(
            'user/devices.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }


    /**
     * @param  Request  $request
     * @param  Device  $device
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/{id}/update-name", name="device_update_name", methods={"POST"})
     */
    public function updateName(Request $request, Device $device): Response
    {
        if (!$device->hasUser($this->getUser())       ) {
            throw new \InvalidArgumentException();
        }

        $device->setName($request->request->get('name'));

        $em = $this->getDoctrine()->getManager();

        $em->persist($device);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

}
