<?php



namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @package AppBundle\Controller\User
 *
 * @Route("/uzytkownik")
 */
class UserController extends Controller
{

    /**
     * @return Response
     *
     * @Route("/", name="user", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render(
            'user/show.html.twig',
            [

            ]
        );
    }
}
