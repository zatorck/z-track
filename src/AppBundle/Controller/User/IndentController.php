<?php



namespace AppBundle\Controller\User;

use AppBundle\Entity\Indent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IntentController
 *
 * @package AppBundle\Controller\User
 *
 * @Route("/uzytkownik")
 */
class IndentController extends Controller
{

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/zamowienia", name="user_indent_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Indent::class)->getByUserIdQuery($this->getUser()->getId());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            50/*limit per page*/
        );

        return $this->render(
            'user/indents.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

}
