<?php


namespace AppBundle\Controller\API;

use AppBundle\AppSettings;
use AppBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 *
 * @package AppBundle\Controller
 *
 * @Route("/api/979878970h89686876879t7gyuyfyfytfytf/user/")
 */
class UserController extends Controller
{

    /**
     * @return Response
     *
     * @Route("new", methods={"POST"})
     */
    public function new(Request $request, \Swift_Mailer $mailer): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:User');
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $plainPassword = '1234'.rand(10000000, 100000000);

        $user = $repository->findOneByEmail($email);
        $userManager = $this->container->get('fos_user.user_manager');


        if (!$user) {
            $user = new User();

            $user->setEmail($email);
            $user->setPlainPassword($plainPassword);
            $user->setEnabled(1);
            $userManager->updateUser($user);
            $em->persist($user);
            $em->flush();
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();
        } else {
            $user->setPlainPassword($plainPassword);
            $user->setEnabled(1);
            $userManager->updateUser($user);
            $em->persist($user);
            $em->flush();
            $user->setPassword($password);
            $em->persist($user);
            $em->flush();
        }


        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @return Response
     *
     * @Route("change-password")
     */
    public function changePassword(Request $request, \Swift_Mailer $mailer,LoggerInterface $logger): Response
    {
        $password = $request->query->get('password');
        $traccarId = $request->query->get('id');
        $logger->critical('Zmieniam haslo');

        if(!$password && !$traccarId){
            $password = $request->request->get('password');
            $traccarId = $request->request->get('id');
        }

        if ($traccarId == 0) {
            return new JsonResponse(['status' => 'success']);
        }


        $userManager = $this->container->get('fos_user.user_manager');


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByTraccarId($traccarId);

        if (!$user) {
            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Nie znaleziono uzytkownika'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo('piotr.zat@gmail.com')
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->renderView(
                                    'emails/admin/userNotFound.html.twig',
                                    ['traccarId' => $traccarId]
                            ),
                            'text/html'
                    );

            $mailer->send($message);

            //--------------- sending mails


            return new JsonResponse(['status' => 'success']);
        }

        $user->setDontUseTraccarApi(true);
        $user->setPlainPassword($password);
        $userManager->updateUser($user);


        return new JsonResponse(['status' => 'success']);
    }


}
