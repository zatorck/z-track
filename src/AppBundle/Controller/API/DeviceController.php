<?php


namespace AppBundle\Controller\API;

use AppBundle\AppSettings;
use AppBundle\Entity\Device;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DeviceController
 *
 * @package AppBundle\Controller
 *
 * @Route("/api/979878970h89686876879t7gyuyfyfytfytf/device/")
 */
class DeviceController extends Controller
{

    /**
     * @param  Request  $request
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Device');
        $serialNumber = $request->request->get('serial_number');
        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $endAt = $request->request->get('end_at');
        $simEndAt = $request->request->get('sim_end_at');
        $simNumber = $request->request->get('sim_number');
        $notice = $request->request->get('notice');
        $user = $em->getRepository('AppBundle:User')->findOneByEmail($email);
        $device = $repository->findOneBySerialNumber($serialNumber);
        if (!$device && $user) {
            $device = new Device();
        }

        $device->setSerialNumber($serialNumber);
        $device->setName($name);
        $device->addUser($user);
        $device->setEndAt(new \DateTime($endAt));
        $device->setSimNumber($simNumber);
        $device->setNotice($notice);
        $device->setSimEndAt(new \DateTime($simEndAt));
        $device->setConfigured(false);

        $em->persist($device);
        $em->flush();

        return new JsonResponse(['status' => 'success']);
    }

}
