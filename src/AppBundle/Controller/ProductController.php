<?php



namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{

    /**
     * @param Product $product
     *
     * @return Response
     *
     * @Route("/produkt/{slug}", name="product", methods={"GET"})
     */
    public function product(Product $product): Response
    {
        return $this->render(
            'product/show.html.twig',
            [
                'product' => $product,
            ]
        );
    }

    /**
     * @param int|null $id
     * @param Product|null $product
     *
     * @return Response
     */
    public function mainImageAction(int $id = null, Product $product = null): Response
    {
        if (!$product) {
            $product = $this->getDoctrine()->getManager()->getRepository(
                'AppBundle:Product'
            )->find($id);
        }

        return $this->render(
            'product/_mainImage.html.twig',
            [
                'product' => $product,
            ]
        );
    }

}
