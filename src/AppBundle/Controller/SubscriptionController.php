<?php



namespace AppBundle\Controller;

use AppBundle\Entity\Subscription;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SubscriptionController
 *
 * @package AppBundle\Controller
 */
class SubscriptionController extends Controller
{

    /**
     * @return Response
     *
     * @Route("/abonamenty/", name="subscription_index", methods={"GET"})
     */
    public function index(): Response
    {

        if($this->getUser()){
            return $this->redirectToRoute('indent_create_with_subscription');
        }

        $em = $this->getDoctrine()->getManager();

        $subscriptions = $em->getRepository('AppBundle:Subscription')->findAllActive();

        return $this->render(
                'subscription/index.html.twig',
                [
                        'subscriptions' => $subscriptions,
                ]
        );
    }





}
