<?php


namespace AppBundle\Controller;

use AppBundle\AppSettings;
use AppBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AppController
 *
 * @package AppBundle\Controller
 */
class AppController extends Controller
{

    /**
     * @return Response
     *
     * @Route("/", name="home", methods={"GET"})
     */
    public function home(): Response
    {
        $em = $this->getDoctrine();
        $subscriptions = $em->getRepository('AppBundle:Subscription')->findAllActive();

        return $this->render(
                'base.html.twig',
                [
                        'subscriptions' => $subscriptions,
                ]
        );
    }

    /**
     * @return Response
     *
     * @Route("/najczesciej-zadawane-pytania", name="faqs", methods={"GET"})
     */
    public function faqs(): Response
    {

        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('AppBundle:Faq')->getAllQuery();


        return $this->render(
                'faq/index.html.twig',
                [
                        'faqs' => $query->getResult(),
                ]
        );
    }

    /**
     * @return Response
     *
     * @Route("/regulamin", name="regulations", methods={"GET"})
     */
    public function regulations(): Response
    {
        return $this->render(
                'regulations.html.twig',
                [
                ]
        );
    }

    /**
     * @return Response
     *
     * @Route("/kontakt", name="contact", methods={"GET", "POST"})
     */
    public function contact(Request $request, \Swift_Mailer $mailer): Response
    {

        $contact = new Contact();
        $form = $this->createForm('AppBundle\Form\ContactType', $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($this->getUser()) {
                $contact->setUser($this->getUser());
            }

            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Do admina -  Nowy kontakt'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo(AppSettings::ADMIN_EMAIL)
                    ->setReplyTo($contact->getEmail())
                    ->setBody(
                            $this->renderView(
                                    'emails/admin/contact.html.twig',
                                    ['contact' => $contact]
                            ),
                            'text/html'
                    );

            $mailer->send($message);
            //--------------- sending mails


            $em->persist($contact);
            $em->flush();
            $this->addFlash('info', 'Dziękujemy za kontakt postaramy się odpowiedzieć jak najszybciej!');
        }

        return $this->render(
                'contact.html.twig',
                [
                        'form' => $form->createView(),
                ]
        );
    }

    /**
     * @return Response
     *
     * @Route("/profile/", name="user_profile_show_redirect", methods={"GET"})
     */
    public function showRedirect(): Response
    {
        return $this->redirectToRoute('user');
    }

    /**
     * @return Response
     *
     * @Route("/profile/edit", name="user_profile_edit_redirect", methods={"GET"})
     */
    public function editRedirect(): Response
    {
        return $this->redirectToRoute('user');
    }

    /**
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @Route("/sklep", name="shop", methods={"GET"})
     */
    public function shop(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->getFirstQuery()
                ->getOneOrNullResult();

        return $this->redirectToRoute(
                'category',
                [
                        'slug' => $category->getSlug(),
                ]
        );
    }

    /**
     * @return Response
     *
     * @Route("/panel", name="redirect_to_panel", methods={"GET"})
     */
    public function redirectToPanel()
    {
        return $this->redirect('https://panel.z-track.com/?token='.$this->getUser()->getTraccarToken().'&time='.date('ymdhis'));
    }

    /**
     * @return Response
     *
     * @Route("/demo", name="redirect_to_demo", methods={"GET"})
     */
    public function redirectToDemo()
    {
        return $this->redirect('https://panel.z-track.com/?token=b3779f60c973c3a412bba6bc424236f37be731bc9a7ca19aac07cbb35909fb594771a87b7b9523ff7502298fb4c46daf&time='.date('ymdhis'));
    }

    /**
     * @return Response
     *
     * @Route("/p",  methods={"GET"})
     */
    public function p()
    {
        return $this->redirectToRoute('redirect_to_panel');
    }

    /**
     * @return Response
     *
     * @Route("/cp",  methods={"GET"})
     */
    public function cp()
    {
        return $this->redirectToRoute('redirect_to_panel');
    }


    /**
     * @return Response
     */
    public function renderNavAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('AppBundle:Category')->getActiveQuery()
                ->getArrayResult();

        return $this->render(
                '_header.html.twig',
                [
                        'categories' => $categories,
                ]
        );
    }
}
