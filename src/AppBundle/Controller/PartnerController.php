<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Partner;
use AppBundle\Repository\PartnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PartnerController
 *
 * @package AppBundle\Controller
 */
class PartnerController extends Controller
{
    /**
     * @param Partner $partner
     *
     * @return Response
     *
     * @Route("/przedstawiciele-handlowi/", name="partners", methods={"GET"})
     */
    public function index(EntityManagerInterface $em): Response
    {
        $partners = $em->getRepository('AppBundle:Partner')->findAllForIndex();

        return $this->render(
            'partner/index.html.twig',
            [
                'partners' => $partners,
            ]
        );
    }

    /**
     * @param Partner $partner
     *
     * @return Response
     *
     * @Route("/przedstawiciel-handlowy/{slug}", name="partner", methods={"GET"})
     */
    public function show(Partner $partner): Response
    {
        return $this->render(
            'partner/show.html.twig',
            [
                'partner' => $partner,
            ]
        );
    }

}
