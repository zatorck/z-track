<?php



namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 *
 * @package AppBundle\Controller
 */
class CategoryController extends Controller
{

    /**
     * @param Category $category
     *
     * @return Response
     *
     * @Route("/kategoria/{slug}", name="category", methods={"GET"})
     */
    public function products(Category $category): Response
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('AppBundle:Product')
            ->getActiveByCatergoryIdArrayQuery($category->getId())->getArrayResult();

        return $this->render(
            'category/show.html.twig',
            [
                'products' => $products,
                'category' => $category,
            ]
        );
    }

}
