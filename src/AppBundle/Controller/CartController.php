<?php



namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Helper\Session\CartCleanerHelper;
use AppBundle\Helper\Session\CartHelper;
use AppBundle\Utils\Serializer\JsonDecoder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 */
class CartController extends Controller
{

    /**
     * @param  CartCleanerHelper  $cartCleaner
     *
     * @return Response
     *
     * @Route("/koszyk", name="cart", methods={"GET"})
     */
    public function show(CartCleanerHelper $cartCleaner): Response
    {
        $cartCleaner->compareCartItems();

        return $this->render(
                'cart.html.twig'
        );
    }

    /**
     * @param  Product  $product
     * @param  Request  $request
     * @param  CartHelper  $helper
     *
     * @return Response
     *
     * @Route("/dodaj-produkt-do-koszyka/{id}", name="add_to_cart", methods={"GET"})
     */
    public function products(
            Product $product,
            Request $request,
            CartHelper $helper
    ): Response {
        $count = (int) $request->query->get('count', 1);
        $helper->addProductToCart($product, $count);
        if ($request->query->get('buy_now')) {
            $response = $this->redirectToRoute('indent_create');
        } else {
            $this->addFlash('info', 'Produkt został dodany do koszyka!');

            $response = $this->redirectToRoute('cart');
        }

        return $response;
    }

    /**
     * @param  Request  $request
     * @param  JsonDecoder  $jsonDecoder
     * @param  CartHelper  $helper
     *
     * @return Response
     *
     * @Route("/aktualizuj-koszyk", name="update_cart_from_string", methods={"GET", "POST"})
     */
    public function updateCartFromString(
            Request $request,
            JsonDecoder $jsonDecoder,
            CartHelper $helper
    ): Response {
        $json = $request->get('string', '[]');
        $items = $jsonDecoder->decode($json);
        $helper->updateCartFromArray($items);
        $this->addFlash('info', 'Koszyk został edytowany!');
        $response = $this->redirectToRoute('cart');

        return $response;
    }

}
