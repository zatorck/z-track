<?php


namespace AppBundle\Helper\LifecycleEventArgs;


use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * Class ChangeSetHelper
 * @package AppBundle\Helper\LifecycleEventArgs
 */
class ChangeSetHelper
{
    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    public function getEntityChangeSet(LifecycleEventArgs $args)
    {
        return $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
    }
}
