<?php



namespace AppBundle\Helper\Session;

use AppBundle\Fetcher\ProductFetcher;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CartCleanerHelper
 *
 * @package AppBundle\Helper\Session
 */
class CartCleanerHelper
{

    /**
     * @var CartHelper
     */
    private $helper;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ProductFetcher
     */
    private $productFetcher;

    /**
     * CartChecker constructor.
     *
     * @param CartHelper       $helper
     * @param SessionInterface $session
     * @param ProductFetcher   $productFetcher
     */
    public function __construct(
        CartHelper $helper,
        SessionInterface $session,
        ProductFetcher $productFetcher
    ) {
        $this->helper = $helper;
        $this->session = $session;
        $this->productFetcher = $productFetcher;
    }

    /**
     * @return bool
     */
    public function compareCartItems(): bool
    {
        if ($this->helper->checkIfCartIsNull()) {
            return true;
        }

        $ids = $this->helper->getProductIdsArray();
        if (count($ids) == 0) {
            return true;
        }

        $products = $this->productFetcher->fetchProductsFromArrayOfIds($ids);

        $cart = $this->helper->getCurrentCart();

        $valid = true;

        if ($this->removeRemovedProductsFromCart($cart, $products) != 0) {
            $valid = false;
        }

        if ($this->removeChangedProducts($cart, $products) != 0) {
            $valid = false;
        }

        $this->helper->saveCart();

        return $valid;
    }

    /**
     * @param array $cart
     * @param array $products
     *
     * @return int
     */
    private function removeRemovedProductsFromCart(array $cart, array $products): int
    {
        $count = 0;
        foreach ($cart['products'] as $product) {
            $found = false;
            foreach ($products as $fetchedProduct) {
                if ($product['id'] == $fetchedProduct->getId()
                    && $fetchedProduct->getActive()
                ) {
                    $found = true;
                }
            }
            if (!$found) {
                $count++;
                $this->helper->removeProductFromCart($product['id']);
            }
        }
        if ($count > 0) {
            $this->session->getFlashBag()->add(
                'warning',
                'Produkt znajdujący się w Twojej karcie został usunięty ze sklepu, został usunięty również z Twojej karty. Łącza liczba produktów: '
                .$count
            );
        }

        return $count;
    }

    /**
     * @param array $cart
     * @param array $products
     *
     * @return int
     */
    private function removeChangedProducts(array $cart, array $products): int
    {
        $count = 0;
        foreach ($products as $product) {
            $fetchedUpdatedAt = $product->getUpdatedAt();
            $cartUpdatedAt = $cart['products'][$product->getId()]['product']->getUpdatedAt();
            if ($cartUpdatedAt != $fetchedUpdatedAt) {
                $count++;
                $this->helper->removeProductFromCart($product->getId());
            }
        }
        if ($count > 0) {
            $this->session->getFlashBag()->add(
                'warning',
                'Produkt znajdujący się w Twojej karcie został edytowany, dlatego został usunięty z Twojej karty. Łączna liczba produktów: '
                .$count
            );
        }

        return $count;
    }

}
