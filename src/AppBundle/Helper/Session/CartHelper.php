<?php



namespace AppBundle\Helper\Session;

use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CartHelper
 *
 * @package AppBundle\Helper\Session
 */
class CartHelper
{

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var array|null
     */
    private $cart;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->cart = $session->get('cart');
    }

    /**
     * @param  Product  $product
     * @param  int  $count
     *
     * @return array
     */
    public function addProductToCart(Product $product, int $count): array
    {
        $added = false;
        if ($this->checkIfCartIsNull()) {
            $this->addProductToEmptyCart($product, $count);
            $added = true;
        } else {
            if ($this->checkIfProductInCart($product)) {
                $this->addCountToProductInCart($product, $count);
                $added = true;
            }
        }

        if (!$added) {
            $this->cart['count']++;

            $this->cart['products'][$product->getId()] = [
                'count' => $count,
                'id' => $product->getId(),
                'product' => $product,
            ];
        }
        $this->addTotalToCart();
        $this->saveCart();

        return $this->cart;
    }

    /**
     * @return boolean
     */
    public function checkIfCartIsNull(): bool
    {
        if ($this->cart == null) {
            return true;
        }

        return false;
    }

    /**
     * @param Product $product
     * @param int     $count
     */
    private function addProductToEmptyCart(Product $product, int $count): void
    {
        $this->cart = [
            'count' => 1,
            'products' => [
                $product->getId() => [
                    'count' => $count,
                    'id' => $product->getId(),
                    'product' => $product,
                ],
            ],
        ];
    }

    /**
     * @param Product $product
     *
     * @return boolean
     */
    private function checkIfProductInCart(Product $product): bool
    {
        foreach ($this->cart['products'] as $cartProduct) {
            if ($cartProduct['id'] == $product->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Product $product
     * @param int     $count
     */
    private function addCountToProductInCart(Product $product, int $count): void
    {
        $this->cart['products'][$product->getId()]['count']
            = (int)$this->cart['products'][$product->getId()]['count'] + $count;
    }

    /**
     * addTotalToCart
     */
    private function addTotalToCart(): void
    {
        $total = 0;
        foreach ($this->cart['products'] as $item) {

            // Checking product instance
            if (!($item['product'] instanceof Product)) {
                throw new \InvalidArgumentException();
            }

            $total = $total + $item['count'] * $item['product']->getPrice();
        }
        $this->cart['total'] = $total;
    }

    /**
     * Save cart function
     */
    public function saveCart(): void
    {
        $this->session->set('cart', $this->cart);
    }

    /**
     * @return mixed
     */
    public function getCurrentCart(): ?array
    {
        return $this->cart;
    }

    /**
     * @return array
     */
    public function getProductIdsArray(): array
    {
        $array = [];
        if (!$this->checkIfCartIsNull()) {
            foreach ($this->cart['products'] as $cartProduct) {

                // Checking product instance
                if (!($cartProduct['product'] instanceof Product)) {
                    throw new \InvalidArgumentException();
                }

                $array[] = $cartProduct['product']->getId();
            }
        }

        return $array;
    }

    /**
     * @param array $items
     */
    public function updateCartFromArray(array $items): void
    {
        foreach ($items as $item) {
            $this->cart['products'][$item['id']]['count'] = $item['count'];
        }

        foreach ($this->cart['products'] as $product) {
            $found = false;
            foreach ($items as $item) {
                if ($item['id'] == $product['id']) {
                    $found = true;
                }
            }
            if (!$found) {
                $this->removeProductFromCart($product['id']);
            }
        }
        $this->addTotalToCart();
        $this->saveCart();
    }

    /**
     * @param int $id
     */
    public function removeProductFromCart(int $id): void
    {
        if (isset($this->cart['products'][$id])) {
            $this->cart['count']--;
            $this->addTotalToCart();
            unset($this->cart['products'][$id]);
        }

    }

}
