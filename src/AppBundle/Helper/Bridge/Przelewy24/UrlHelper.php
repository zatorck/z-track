<?php



namespace AppBundle\Helper\Bridge\Przelewy24;

use Allset\Przelewy24Bundle\Factory\ProcessFactory;
use AppBundle\Entity\Indent;
use AppBundle\Factory\Przelewy24\PaymentFactory;

/**
 * Class UrlHelper
 *
 * @package AppBundle\Helper\Bridge
 */
class UrlHelper
{
    /**
     * @var PaymentFactory
     */
    private $paymentFactory;
    /**
     * @var ProcessFactory
     */
    private $processFactory;

    /**
     * UrlHelper constructor.
     *
     * @param PaymentFactory $paymentFactory
     * @param ProcessFactory $processFactory
     */
    public function __construct(PaymentFactory $paymentFactory, ProcessFactory $processFactory)
    {
        $this->processFactory = $processFactory;
        $this->paymentFactory = $paymentFactory;
    }

    /**
     * @param Indent $indent
     *
     * @return string
     * @throws \Exception
     */
    public function getUrl(Indent $indent): string
    {
        $payment = $this->paymentFactory->create($indent);
        $this->processFactory->setPayment($payment);
        $url = $this->processFactory->createAndGetUrl();

        return $url;
    }
}
