<?php


namespace AppBundle\Command;


use AppBundle\Utils\Traccar\UserSync;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class TraccarSyncUsersCommand
 *
 * @package AppBundle\Command
 */
class TraccarSyncUsersCommand extends Command
{
    /**
     * the name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'traccar:users:sync';

    /**
     * @var UserSync
     */
    private $userSync;

    /**
     * TraccarSyncUsersCommand constructor.
     *
     * @param  UserSync  $userSync
     * @param  null  $name
     */
    public function __construct(UserSync $userSync, $name = null)
    {
        $this->userSync = $userSync;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->addOption('from-app', 'f', InputOption::VALUE_NONE, 'służy do synchro z applikacji do traccara');
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     *
     * @return int|void|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fromApp = $input->getOption('from-app');
        $output->writeln('Syncing ... ');
        $this->userSync->sync($fromApp);
        $output->writeln('ok!');

    }
}

