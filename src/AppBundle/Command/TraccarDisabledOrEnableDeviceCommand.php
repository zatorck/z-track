<?php


namespace AppBundle\Command;


use AppBundle\Utils\Traccar\DeviceTouch;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class TraccarDisabledOrEnableDeviceCommand
 *
 * @package AppBundle\Command
 */
class TraccarDisabledOrEnableDeviceCommand extends Command
{
    /**
     * the name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'traccar:devices:disable-or-enable';

    /**
     * @var DeviceTouch
     */
    private $deviceTouch;

    /**
     * TraccarDisabledOrEnableDeviceCommand constructor.
     *
     * @param  DeviceTouch  $deviceTouch
     * @param  null  $name
     */
    public function __construct(DeviceTouch $deviceTouch, $name = null)
    {
        $this->deviceTouch = $deviceTouch;
        parent::__construct($name);
    }


    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     *
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Syncing ... ');
        $this->deviceTouch->touchToDeactiveOrActive();
        $output->writeln('ok!');

    }
}

