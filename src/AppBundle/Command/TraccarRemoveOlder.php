<?php


namespace AppBundle\Command;


use AppBundle\Utils\Traccar\RemoveOlder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class TraccarRemoveOlder extends Command
{
    /**
     * the name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'traccar:delete-zeros';

    /**
     * @var RemoveOlder
     */
    private $removeOlder;

    /**
     * TraccarremoveOlderCommand constructor.
     *
     * @param  RemoveOlder  $removeOlder
     * @param  null  $name
     */
    public function __construct(RemoveOlder $removeOlder, $name = null)
    {
        $this->removeOlder = $removeOlder;
        parent::__construct($name);
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Deleting ... ');
        $this->removeOlder->run(269);
        $output->writeln('ok!');
    }
}

