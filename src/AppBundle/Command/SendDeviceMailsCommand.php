<?php


namespace AppBundle\Command;


use AppBundle\AppSettings;
use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceMail;
use AppBundle\Repository\DeviceMailRepository;
use AppBundle\Repository\DeviceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class SendDeviceMailsCommand
 *
 * @package AppBundle\Command
 */
class SendDeviceMailsCommand extends Command
{
    /**
     * the name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'app:device:send-emails';


    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * SendDeviceMailsCommand constructor.
     *
     * @param  DeviceRepository  $deviceRepository
     * @param  DeviceMailRepository  $deviceMailRepository
     * @param  EntityManagerInterface  $em
     * @param  null  $name
     */
    public function __construct(
            EntityManagerInterface $em,
            \Swift_Mailer $mailer,
            \Twig_Environment $templating,
            $name = null
    ) {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->templating = $templating;
        parent::__construct($name);
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     *
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Sending ... ');
        $repo = $this->em->getRepository('AppBundle:Device');
        $today = new \DateTime('today');
        $tommorow = clone $today;
        $tommorow->modify('+1 day');
        $daysLeftArray = [3, 7, 14];

        $devicesInformed = [];
        foreach ($this->em->getRepository('AppBundle:DeviceMail')->findAllToday() as $deviceMail) {
            $devicesInformed[$deviceMail['id']] = $deviceMail['id'];
        }


        foreach ($daysLeftArray as $daysLeft) {
            $devices = $repo->findToSendEmail(
                    $today->modify('+'.$daysLeft.' days'),
                    $tommorow->modify('+'.$daysLeft.' days'),
                    $devicesInformed
            );
            $today->modify('-'.$daysLeft.' days');
            $tommorow->modify('-'.$daysLeft.' days');

            foreach ($devices as $device) {
                if (!($device instanceof Device)) {
                    continue;
                }

                $deviceMail = new DeviceMail();
                $deviceMail->setDaysLeft($daysLeft);
                $deviceMail->setDevice($device);
                $this->em->persist($deviceMail);


                //--------------- sending mails
                foreach ($device->getUsers() as $user) {
                    $message = (new \Swift_Message('Za '.$daysLeft.' kończy się Twój abonament na urządzenie: '.$device->getSerialNumber()))
                            ->setFrom('noreply@z-track.com', 'z-Track')
                            ->setTo($user->getEmail())
                            ->setReplyTo(AppSettings::ADMIN_EMAIL)
                            ->setBody(
                                    $this->templating->render(
                                            'emails/user/subscriptionEnding.html.twig',
                                            [
                                                    'device' => $device,
                                                    'daysLeft' => $daysLeft,
                                            ]
                                    ),
                                    'text/html'
                            );

                    $this->mailer->send($message);
                }
                //--------------- sending mails

                //--------------- sending mails
                $message = (new \Swift_Message('Do admina - Za '.$daysLeft.' kończy się  abonament na urządzenie: '.$device->getSerialNumber()))
                        ->setFrom('noreply@z-track.com', 'z-Track')
                        ->setTo(AppSettings::ADMIN_EMAIL)
                        ->setReplyTo(AppSettings::ADMIN_EMAIL)
                        ->setBody(
                                $this->templating->render(
                                        'emails/admin/subscriptionEnding.html.twig',
                                        [
                                                'device' => $device,
                                                'daysLeft' => $daysLeft,
                                        ]
                                ),
                                'text/html'
                        );

                $this->mailer->send($message);
                //--------------- sending mails


            }

            $this->em->flush();
            $this->em->clear();
        }


        $output->writeln('ok!');
    }
}

