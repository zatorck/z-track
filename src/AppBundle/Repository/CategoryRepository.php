<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class CategoryRepository
 *
 * @package AppBundle\Repository
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

    /**
     * @return Query
     */
    public function getFirstQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.position', 'ASC')
            ->setMaxResults(1)
            ->getQuery();
    }

    /**
     * @return Query
     */
    public function getActiveQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->where('a.active = :true')
            ->setParameter('true', true)
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

}
