<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class SubscriptionRepository
 *
 * @package AppBundle\Repository
 */
class SubscriptionRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a')
                ->orderBy('a.months', 'ASC')
                ->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllActive()
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a')
                ->orderBy('a.months', 'ASC')
                ->where('a.active = :true')
                ->setParameter('true', true)
                ->getQuery()
                ->getResult();
    }

}
