<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class ShippingRepository
 *
 * @package AppBundle\Repository
 */
class ShippingRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.id', 'ASC')
            ->getQuery();
    }
}
