<?php


namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class IndentRepository
 *
 * @package AppBundle\Repository
 */
class IndentRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Query
     */
    public function getAllQuery(?string $query = null): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a', 'shipping', 'shippingAddress', 'user')
                ->leftJoin('a.shipping', 'shipping')
                ->leftJoin('a.shippingAddress', 'shippingAddress')
                ->leftJoin('a.user', 'user')
                ->leftJoin('a.indentSubscriptions', 'indentSubscriptions')
                ->leftJoin('indentSubscriptions.device', 'device')
                ->andWhere(
                        $qb->expr()->orX(
                                $qb->expr()->like(
                                        'a.number',
                                        $qb->expr()->literal('%'.$query.'%')
                                ),
                                $qb->expr()->like(
                                        'device.serialNumber',
                                        $qb->expr()->literal('%'.$query.'%')
                                ),
                                $qb->expr()->like(
                                        'user.email',
                                        $qb->expr()->literal('%'.$query.'%')
                                )
                        )
                )
                ->andWhere('a.tempEnded = :true')
                ->setParameter('true', true)
                ->orderBy('a.id', 'DESC')
                ->getQuery();
    }

    /**
     * @param  int  $userId
     *
     * @return Query
     */
    public function getByUserIdQuery(int $userId): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a', 'shipping', 'shippingAddress')
                ->leftJoin('a.shipping', 'shipping')
                ->leftJoin('a.shippingAddress', 'shippingAddress')
                ->where('a.user = :userId')
                ->setParameter('userId', $userId)
                ->andWhere('a.tempEnded = :true')
                ->setParameter('true', true)
                ->orderBy('a.id', 'DESC')
                ->getQuery();
    }
}
