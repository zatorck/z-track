<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class AddressRepository
 *
 * @package AppBundle\Repository
 */
class AddressRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.id', 'ASC')
            ->getQuery();
    }
}
