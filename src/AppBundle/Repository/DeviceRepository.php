<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * DeviceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DeviceRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Query
     */
    public function getAllQuery(?string $query = null): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a', 'users')
                ->leftJoin('a.users', 'users')
                ->andWhere(
                        $qb->expr()->like(
                                'a.serialNumber',
                                $qb->expr()->literal('%'.$query.'%')
                        )
                )
                ->orWhere(
                        $qb->expr()->like(
                                'users.email',
                                $qb->expr()->literal('%'.$query.'%')
                        )
                )
                ->orWhere(
                        $qb->expr()->like(
                                'a.name',
                                $qb->expr()->literal('%'.$query.'%')
                        )
                )
                ->orWhere(
                        $qb->expr()->like(
                                'a.simNumber',
                                $qb->expr()->literal('%'.$query.'%')
                        )
                )
                ->orderBy('a.id', 'DESC')
                ->getQuery();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function findToDisableOrEnable(): array
    {
        $qb = $this->createQueryBuilder('a');
        $now = new \DateTime();

        $qb
                ->select('a')
                ->where($qb->expr()->orX(
                        $qb->expr()->andX(
                                'a.endAt > :now', 'a.disabledInTraccar = :true'
                        ),
                        $qb->expr()->andX(
                                'a.endAt < :now', 'a.disabledInTraccar = :false'
                        )
                ))
//                ->andWhere('a.serialNumber = :sss')
                ->setParameters([
                        'now' => $now,
                        'true' => true,
                        'false' => false,
//                        'sss' => 3333,
                ]);


        return $qb
                ->getQuery()
                ->getResult();
    }

    /**
     * @param  int  $userId
     *
     * @return Query
     */
    public function getByUserIdQuery(int $userId): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
                ->select('a', 'users')
                ->leftJoin('a.users', 'users')
                ->where('users.id = :userId')
                ->setParameter('userId', $userId)
                ->orderBy('a.id', 'DESC')
                ->getQuery();
    }

    /**
     * @param $dateMin
     * @param $dateMax
     *
     * @return Query
     */
    public function findToSendEmail($dateMin, $dateMax, $devicesInformed)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
                ->select('a')
                ->where('a.endAt >= :dateMin')
                ->andWhere('a.endAt <= :dateMax');

        if (count($devicesInformed)) {
            $qb
                    ->andWhere($qb->expr()->notIn('a.id', $devicesInformed));
        }

        return $qb
                ->setParameters([
                        'dateMax' => $dateMax,
                        'dateMin' => $dateMin,
                ])
                ->getQuery()
                ->getResult();
    }

    public function getEndingQuery()
    {

        $date = new \DateTime('-31 days');

        $qb = $this->createQueryBuilder('a');

        $qb
                ->select('a')
                ->where('a.simEndAt >= :date');

        return $qb
                ->setParameters([
                        'date' => $date,
                ])
                ->orderBy('a.simEndAt', 'ASC')
                ->getQuery();


    }
}
