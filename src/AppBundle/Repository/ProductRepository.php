<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class ProductRepository
 *
 * @package AppBundle\Repository
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

    /**
     * @return Query
     */
    public function getActiveQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->where('a.active = :true')
            ->setParameter('true', true)
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

    /**
     * @param int $categoryId
     *
     * @return Query
     */
    public function getActiveByCatergoryIdArrayQuery(int $categoryId): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a', 'ac')
            ->leftJoin('a.category', 'ac')
            ->where('a.active = :true')
            ->setParameter('true', true)
            ->andWhere('ac.id = :categoryId')
            ->setParameter('categoryId', $categoryId)
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

    /**
     * @param array $ids
     *
     * @return Query
     */
    public function getProductsFromArrayOfIdsQuery(array $ids): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->where($qb->expr()->in('a.id', $ids))
            ->orderBy('a.position', 'ASC')
            ->getQuery();
    }

}
