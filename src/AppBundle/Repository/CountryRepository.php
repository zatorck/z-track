<?php



namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * Class CountryRepository
 *
 * @package AppBundle\Repository
 */
class CountryRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a')
            ->orderBy('a.id', 'ASC')
            ->getQuery();
    }
}
