<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * DeviceHistoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DeviceHistoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Query
     */
    public function getAllQuery(): Query
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select('a', 'user')
            ->leftJoin('a.user', 'user')
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }
}
