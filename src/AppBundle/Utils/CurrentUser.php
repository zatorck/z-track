<?php

namespace AppBundle\Utils;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CurrentUser
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * IndentCreateSubscriber constructor.
     *
     * @param  TokenStorageInterface  $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
    }

    public function getCurrentUser(): ?User
    {
        if (is_string($this->user)) {
            $this->user = null;
        }
        return $this->user;
    }
}
