<?php



namespace AppBundle\Utils\Serializer;

/**
 * Class JsonEncoder
 *
 * @package AppBundle\Serializer\Data
 */
class JsonEncoder
{

    /**
     * @param array $data
     *
     * @return string
     */
    public function encode(array $data): string
    {
        $jsonContent = json_encode($data);

        return $jsonContent;
    }

}
