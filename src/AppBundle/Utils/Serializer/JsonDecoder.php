<?php



namespace AppBundle\Utils\Serializer;

/**
 * Class JsonDecoder
 *
 * @package AppBundle\Serializer
 */
class JsonDecoder
{

    /**
     * @param  string  $string
     *
     * @return array
     */
    public function decode(string $string): array
    {
        $data = json_decode($string, true);

        return $data;
    }

}
