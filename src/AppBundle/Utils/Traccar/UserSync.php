<?php


namespace AppBundle\Utils\Traccar;


use AppBundle\Entity\Device;
use AppBundle\Entity\User;
use AppBundle\Request\Api\DeviceAddRequest;
use AppBundle\Request\Api\DeviceListRequest;
use AppBundle\Request\Api\UserAddRequest;
use AppBundle\Request\Api\UserListRequest;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserSync
 *
 * @package AppBundle\Utils\Traccar
 */
class UserSync
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserListRequest
     */
    private $userListRequest;

    /**
     * @var DeviceListRequest
     */
    private $deviceListRequest;

    /**
     * @var UserAddRequest
     */
    private $userAddRequest;

    /**
     * @var DeviceAddRequest
     */
    private $deviceAddRequest;

    /**
     * UserSync constructor.
     *
     * @param  UserListRequest  $userListRequest
     * @param  EntityManagerInterface  $entityManager
     */
    public function __construct(
            UserListRequest $userListRequest,
            DeviceListRequest $deviceListRequest,
            EntityManagerInterface $entityManager,
            UserAddRequest $userAddRequest,
            DeviceAddRequest $deviceAddRequest
    ) {
        $this->userListRequest = $userListRequest;
        $this->deviceListRequest = $deviceListRequest;
        $this->entityManager = $entityManager;
        $this->userAddRequest = $userAddRequest;
        $this->deviceAddRequest = $deviceAddRequest;
    }

    /**
     * @param  bool  $syncToTraccar
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sync(bool $syncToTraccar = false): void
    {
        $this->syncFromTraccar();
        $this->entityManager->clear();
        if ($syncToTraccar) {
            $this->syncToTraccar();
        }
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncFromTraccar()
    {
        $counter = 0;
        foreach ($this->userListRequest->send() as $traccarUser) {
            if ($traccarUser['email'] == 'admin') {
                continue;
            }
            $user = $this->entityManager->getRepository('AppBundle:User')->findOneByEmail($traccarUser['email']);

            if (!$user) {
                $user = $this->entityManager->getRepository('AppBundle:User')->findOneByTraccarId($traccarUser['id']);
            }

            if (!$user) {
                $user = new User();
                $user->setPlainPassword(md5(rand(0, 1000000000).date('Y-m-d H:i:s')));
                $user->setEnabled(true);
            }
            $user->setTraccarId($traccarUser['id']);
            $user->setEmail($traccarUser['email']);
            $user->setTraccarAdministrator($traccarUser['administrator']);
            $user->setTraccarDeviceReadonly($traccarUser['deviceReadonly']);
            $user->setTraccarReadonly($traccarUser['readonly']);
            $user->setTraccarToken($traccarUser['token']);
            $user->setDontUseTraccarApi(true);
            foreach ($this->deviceListRequest->setUser($user)->send() as $traccarDevice) {
                $device = $this->entityManager->getRepository('AppBundle:Device')->findOneBySerialNumber($traccarDevice['uniqueId']);
                if (!$device) {
                    $device = new Device();
                    $device->setEndAt(new \DateTime('+1 month'));
                    $device->setDisabledInTraccar(false);
                }
                $device->setTraccarId($traccarDevice['id']);
                $device->setSerialNumber($traccarDevice['uniqueId']);
                $device->setName($traccarDevice['name']);
                if (!$device->hasUser($user)) {
                    $device->addUser($user);
                }
                $device->setDontUseTraccarApi(true);
                $this->entityManager->persist($device);
            }

            $this->entityManager->persist($user);

            if ($counter % 500 == 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }

            $counter++;
        }

        $this->entityManager->flush();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncToTraccar()
    {
        $users = $this->entityManager->getRepository('AppBundle:User')->findAll();
        foreach ($users as $user) {
            if ($user->getTraccarId()) {
                $user->setUpdatedAt(new \DateTime());
            } else {
                $response = $this->userAddRequest->setUser($user)->send();
                $user->setTraccarId($response['id']);
            }
            $this->entityManager->persist($user);

            foreach ($user->getDevices() as $device) {
                if ($device->getTraccarId()) {
                    $device->setUpdatedAt(new \DateTime());
                } else {
                    $response = $this->deviceAddRequest->setDevice($device)->send();
                    $device->setTraccarId($response['id']);
                }
                $this->entityManager->persist($device);
            }
        }
        $this->entityManager->flush();
    }
}
