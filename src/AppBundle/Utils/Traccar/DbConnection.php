<?php

namespace AppBundle\Utils\Traccar;

use PDO;

class DbConnection
{
    private $host, $port, $dbName, $user, $password;

    public function __construct($host, $port, $dbName, $user, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->password = $password;
    }

    public function executeQuery($sql)
    {
            $conn = new PDO(
                "mysql:host=".$this->host.";port=".$this->port.";dbname=".$this->dbName,
                $this->user,
                $this->password
            );
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->exec($sql);
    }
}