<?php

namespace AppBundle\Utils\Traccar;

class RemoveOlder
{
    public function __construct(DbConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function run(int $days)
    {
        $this->dbConnection->executeQuery(
            'DELETE FROM tc_positions WHERE servertime < \''.(new \DateTime('-'.$days.' days'))->format('Y-m-d H:i:s').'\''
        );
    }
}