<?php


namespace AppBundle\Utils\Traccar;


use Doctrine\ORM\EntityManagerInterface;

/**
 * Class DeviceTouch
 *
 * @package AppBundle\Utils\Traccar
 */
class DeviceTouch
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DeviceTouch constructor.
     *
     * @param  EntityManagerInterface  $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @throws \Exception
     */
    public function touchToDeactiveOrActive()
    {

        foreach ($this->em->getRepository('AppBundle:Device')->findToDisableOrEnable() as $device) {
            $device->setUpdatedAt(new \DateTime());
            $this->em->persist($device);
        }
        $this->em->flush();
    }
}
