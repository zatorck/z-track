<?php


namespace AppBundle\Utils;


use AppBundle\AppSettings;
use GuzzleHttp\Client;

class ApiUtils
{
    public function getClient()
    {
        $client = new Client([
                'base_uri' => AppSettings::TRACCAR_ENDPOINT,
                'headers' => [
                        'authorization' => 'Basic '.AppSettings::TRACCAR_TOKEN,
                ],
        ]);

        return $client;
    }

}
