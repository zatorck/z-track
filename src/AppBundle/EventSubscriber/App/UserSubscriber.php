<?php


namespace AppBundle\EventSubscriber\App;


use AppBundle\Entity\User;
use AppBundle\Helper\AppHelper;
use AppBundle\Helper\LifecycleEventArgs\ChangeSetHelper;
use AppBundle\Request\Api\UserAddRequest;
use AppBundle\Request\Api\UserEditRequest;
use AppBundle\Request\Api\UserRemoveRequest;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class UserSubscriber
 *
 * @package AppBundle\EventSubscriber\App
 */
class UserSubscriber implements EventSubscriber
{
    /**
     * @var ChangeSetHelper
     */
    private $changeSetHelper;

    /**
     * @var UserAddRequest
     */
    private $userAddRequest;

    /**
     * @var UserRemoveRequest
     */
    private $userRemoveRequest;
    /**
     * @var UserEditRequest
     */
    private $userEditRequest;

    /**
     * UserSubscriber constructor.
     *
     * @param  ChangeSetHelper  $changeSetHelper
     * @param  UserAddRequest  $request
     */
    public function __construct(
        ChangeSetHelper $changeSetHelper,
        UserAddRequest $userAddRequest,
        UserRemoveRequest $userRemoveRequest,
        UserEditRequest $userEditRequest
    ) {
        $this->changeSetHelper = $changeSetHelper;
        $this->userAddRequest = $userAddRequest;
        $this->userRemoveRequest = $userRemoveRequest;
        $this->userEditRequest = $userEditRequest;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postRemove,
        ];
    }

    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $model = $args->getObject();

        if (!($model instanceof User) || $model->isDontUseTraccarApi()) {
            return;
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if ($ip === '91.220.222.209') {
            echo 'dasdasdasdsa';
            die;
        }


        if (!$model->getTraccarToken()) {
            $model->setTraccarToken(AppHelper::generateToken());
        }

        $this->userEditRequest->setUser($model)->send();
    }

    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $model = $args->getObject();
        if (!($model instanceof User) || $model->isDontUseTraccarApi()) {
            return;
        }
        $model->setTraccarToken(AppHelper::generateToken());

        $response = $this->userAddRequest->setUser($model)->send();


        $model->setTraccarId($response['id']);
    }

    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $model = $args->getObject();

        if (!($model instanceof User) || $model->isDontUseTraccarApi()) {
            return;
        }

        $this->userRemoveRequest->setUser($model)->send();
    }


}
