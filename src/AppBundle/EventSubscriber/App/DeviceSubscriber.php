<?php


namespace AppBundle\EventSubscriber\App;


use AppBundle\AppSettings;
use AppBundle\Entity\Device;
use AppBundle\Factory\DeviceHistoryFactory;
use AppBundle\Helper\LifecycleEventArgs\ChangeSetHelper;
use AppBundle\Request\Api\DeviceAddRequest;
use AppBundle\Request\Api\DeviceEditRequest;
use AppBundle\Request\Api\DeviceRemoveRequest;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class DeviceSubscriber
 *
 * @package AppBundle\EventSubscriber\App
 */
class DeviceSubscriber implements EventSubscriber
{

    /**
     * @var ChangeSetHelper
     */
    private $changeSetHelper;

    /**
     * @var DeviceHistoryFactory
     */
    private $deviceHistoryFactory;

    /**
     * @var DeviceAddRequest
     */
    private $deviceAddRequest;
    /**
     * @var DeviceEditRequest
     */
    private $deviceEditRequest;
    /**
     * @var DeviceRemoveRequest
     */
    private $deviceRemoveRequest;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * DeviceSubscriber constructor.
     *
     * @param  ChangeSetHelper  $changeSetHelper
     * @param  DeviceHistoryFactory  $deviceHistoryFactory
     * @param  DeviceAddRequest  $deviceAddRequest
     * @param  DeviceEditRequest  $deviceEditRequest
     * @param  DeviceRemoveRequest  $deviceRemoveRequest
     * @param  \Swift_Mailer  $mailer
     * @param  \Twig_Environment  $templating
     */
    public function __construct(
            ChangeSetHelper $changeSetHelper,
            DeviceHistoryFactory $deviceHistoryFactory,
            DeviceAddRequest $deviceAddRequest,
            DeviceEditRequest $deviceEditRequest,
            DeviceRemoveRequest $deviceRemoveRequest,
            \Swift_Mailer $mailer,
            \Twig_Environment $templating
    ) {
        $this->changeSetHelper = $changeSetHelper;
        $this->deviceHistoryFactory = $deviceHistoryFactory;
        $this->deviceAddRequest = $deviceAddRequest;
        $this->deviceEditRequest = $deviceEditRequest;
        $this->deviceRemoveRequest = $deviceRemoveRequest;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
                Events::prePersist,
                Events::preUpdate,
                Events::postRemove,
        ];
    }

    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\Error
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function preUpdate(LifecycleEventArgs $args)
    {


        $model = $args->getObject();
        if (!($model instanceof Device)  || $model->isDontUseTraccarApi()) {
            return;
        }
        $changeSet = $this->changeSetHelper->getEntityChangeSet($args);
        $this->deviceHistoryFactory->createFromDeviceWithChangeSet($model, $changeSet);


        $this->deviceEditRequest->setDevice($model)->setChangeSet($changeSet)->send();

        $endAt = new \DateTime($model->getEndAt()->format('Y-m-d'));

        $model->setEndAt($endAt);


        if ($model->getChangedDisabledInTraccar()) {
            //--------------- sending mails
            foreach ($model->getUsers() as $user) {
                $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Skończył się abonament na urządzenie'))
                        ->setFrom('noreply@z-track.com', 'z-Track')
                        ->setTo($user->getEmail())
                        ->setReplyTo(AppSettings::ADMIN_EMAIL)
                        ->setBody(
                                $this->templating->render(
                                        'emails/user/subscriptionEnded.html.twig',
                                        ['device' => $model]
                                ),
                                'text/html'
                        );

                $this->mailer->send($message);
            }
            //--------------- sending mails
            //--------------- sending mails
            $message = (new \Swift_Message('['.date('d.m.Y H:i').'] Do admina - Skończył się abonament na urządzenie'))
                    ->setFrom('noreply@z-track.com', 'z-Track')
                    ->setTo(AppSettings::ADMIN_EMAIL)
                    ->setReplyTo(AppSettings::ADMIN_EMAIL)
                    ->setBody(
                            $this->templating->render(
                                    'emails/admin/subscriptionEnded.html.twig',
                                    ['device' => $model]
                            ),
                            'text/html'
                    );

            $this->mailer->send($message);
            //--------------- sending mails

        }


    }

    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $model = $args->getObject();
        if (!($model instanceof Device)) {
            return;
        }
        $changeSet = $this->changeSetHelper->getEntityChangeSet($args);
        $this->deviceHistoryFactory->createFromDeviceWithChangeSet($model, $changeSet);

        $response = $this->deviceAddRequest->setDevice($model)->send();

        $model->setTraccarId($response['id']);

        $endAt = new \DateTime($model->getEndAt()->format('Y-m-d'));

        $endAt->modify('+23 hours');
        $endAt->modify('+59 minutes');


        $model->setEndAt($endAt);
    }


    /**
     * @param  LifecycleEventArgs  $args
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $model = $args->getObject();

        if (!($model instanceof Device)) {
            return;
        }

        $this->deviceRemoveRequest->setDevice($model)->send();
    }


}
