<?php


namespace AppBundle\EventSubscriber\App;

use AppBundle\Entity\User;
use AppBundle\Event\IndentCreateEvent;
use AppBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class IndentCreateSubscriber
 *
 * @package AppBundle\EventListener\App
 */
class IndentCreateSubscriber implements EventSubscriberInterface
{

    /**
     * @var User|null
     */
    private $user;

    /**
     * IndentCreateSubscriber constructor.
     *
     * @param  TokenStorageInterface  $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
                Events::INDENT_CREATE => [
                        ['onIndentCreate', 0],
                ],
        ];
    }

    /**
     * @param  IndentCreateEvent  $event
     *
     * @throws \Exception
     */
    public function onIndentCreate(IndentCreateEvent $event): void
    {
        $indent = $event->getIndent();
        $now = new \DateTime();
        if ($this->user instanceof User) {
            $indent->setEmail($this->user->getEmail());
            $indent->setUser($this->user);
        }
        $indent->setNumber(rand(100, 999).$now->format('YmdHi'));

    }
}
