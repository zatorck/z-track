<?php



namespace AppBundle\Event;

use AppBundle\Entity\Indent;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class IndentCreateEvent
 *
 * @package AppBundle\Event
 */
class IndentCreateEvent extends Event
{

    /**
     * @var Indent
     */
    protected $indent;

    /**
     * IndentCreateEvent constructor.
     *
     * @param Indent $indent
     */
    public function __construct(Indent $indent)
    {
        $this->indent = $indent;
    }

    /**
     * @return Indent
     */
    public function getIndent(): Indent
    {
        return $this->indent;
    }

}
