<?php



namespace AppBundle\Factory;

use AppBundle\Entity\Device;
use AppBundle\Entity\DeviceHistory;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class IndentItemFactory
 *
 * @package AppBundle\Factory
 */
class DeviceHistoryFactory
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DeviceHistoryFactory constructor.
     *
     * @param  TokenStorageInterface  $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }


    /**
     * @param  Device  $device
     * @param  array  $changeSet
     *
     * @return DeviceHistory|bool
     */
    public function createFromDeviceWithChangeSet(Device $device, array $changeSet)
    {
        if (!array_key_exists('endAt', $changeSet)) {
            return false;
        }

        if ($changeSet['endAt'][0] == $changeSet['endAt'][1]) {
            return false;
        }
        $deviceHistory = new DeviceHistory();

        $deviceHistory->setDevice($device);

        $deviceHistory->setFromEndAt($changeSet['endAt'][0]);
        $deviceHistory->setToEndAt($changeSet['endAt'][1]);

        if ($this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof User) {
            $deviceHistory->setUser($this->tokenStorage->getToken()->getUser());
        }


        $this->em->merge($deviceHistory);
        $this->em->flush();

        return $deviceHistory;
    }

}
