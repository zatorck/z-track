<?php

namespace AppBundle\Factory;

use AppBundle\Entity\Indent;
use AppBundle\Entity\IndentItem;
use AppBundle\Entity\Product;
use AppBundle\Helper\Session\CartHelper;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class IndentItemFactory
 *
 * @package AppBundle\Factory
 */
class IndentItemFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CartHelper
     */
    private $cartHelper;

    /**
     * IndentItemFactory constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, CartHelper $cartHelper)
    {
        $this->em = $em;
        $this->cartHelper = $cartHelper;
    }

    /**
     * @param Indent $indent
     *
     * @return array
     */
    public function createGroupFromSession(Indent $indent): array
    {
        $indentItems = [];

        // Iteracja wokół kart z sesji
        foreach ($this->cartHelper->getCurrentCart()['products'] as $indentItemArray) {
            $indentItem = new IndentItem();
            $indentItem->setCount($indentItemArray['count']);
            $indentItem->setPrice($indentItemArray['product']->getPrice());
            $indentItem->setTitle($indentItemArray['product']->getTitle());
            $indentItem->setProduct($this->em->getRepository(Product::class)->find($indentItemArray['id']));
            $indentItem->setIndent($indent);
            $indent->addIndentItem($indentItem);

            //Dodawanie wartości do całości zamówienia
            $indentItems[] = $indentItem;
        }
        return $indentItems;
    }

}
