<?php


namespace AppBundle\Factory;

use AppBundle\AppSettings;
use AppBundle\Entity\Device;
use AppBundle\Entity\Log;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class IndentItemFactory
 *
 * @package AppBundle\Factory
 */
class LogFactory
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * LogFactory constructor.
     *
     * @param  TokenStorageInterface  $tokenStorage
     */
    public function __construct(
            TokenStorageInterface $tokenStorage,
            EntityManagerInterface $em
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    /**
     * @param  string  $type
     * @param  string|null  $content
     *
     * @return Log
     */
    public function create(string $type, ?string $content)
    {
        $log = new Log();

        $log->setContent($content);

        $log->setType($type);
        if ($this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof  User) {
            $log->setUser($this->tokenStorage->getToken()->getUser());
        }
        $this->em->merge($log);

        $this->em->flush();

        return $log;
    }

}
