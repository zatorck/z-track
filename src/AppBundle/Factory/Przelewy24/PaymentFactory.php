<?php



namespace AppBundle\Factory\Przelewy24;

use Allset\Przelewy24Bundle\Model\Payment;
use AppBundle\Entity\Indent;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class PaymentFactory
 *
 * @package AppBundle\Factory\Przelewy24
 */
class PaymentFactory
{

    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param  Indent  $indent
     *
     * @return Payment
     * @throws \Exception
     */
    public function create(Indent $indent): Payment
    {
        $payment = new Payment();
        $payment
                ->setCurrency('PLN')
                ->setSessionId($indent->getToken())//VERY IMPORTANT some unique id from your order in your db
                ->setAmount($indent->getTotal() * 100)
                ->setDescription($indent->getEmail())
                ->setEmail($indent->getEmail())
                ->setReturnUrl(
                        $this->router->generate('indent_return', [], 0)
                ); // use following syntax to genreate absolute url

        return $payment;
    }

}
